本项目使用NutzWk-V5 Mini框架，请gitee直接搜索。

doc 文件夹有系统介绍，辅助售前工作

演示地址：http://www.tcjinxiu.com:8888/platform/login admin 123456

演示地址中的wrod生成功能都无法查看，没有多余的服务器搭建office online

讲解视频：
https://www.bilibili.com/video/BV1xvadeuEce/?vd_source=4decc58dc2307af8427290f7a58a0344

源码中没有数据库，不接受白嫖。放出来是为了有需要的同志合作，可以按需收取费用。 支持定制开发

开发周期6个月（已有案例，系统功能都趋于稳定，bug率低）
系统功能：样品登记、样品移交、任务分配、任务认领、任务撤回、报告编制、报告审核、报告批准、报告延期、报告解锁、报告打印、报告收费、
报告及时率、报告综合查询、检验项目配置、报告模板配置、电子签名、特殊字符、厂家管理、设备综合管理
章实现：报告认证章、二维码、批准章、骑缝章的自动插入。（图片形式，非电子签章）

项目使用office online+poi操作word ，支持在线编辑word，不依赖客户端office环境，
目前兼容office制作模板，不兼容wps，支持质检、计量模板自定义。
其中首页模板和附页模板是分开的，在生成时自动合并
首页模板为公共模板，只有一份，
附页模板为私有，根据不同的科室、产品类型上传，生成word时根据样品信息自动匹配，自动带入检验项目，线上编辑


出具报告整体流程图：

![lct.jpg](doc%2Flct.jpg)

功能清单
![img.png](doc%2Fimg.png)
功能展示
![bzfy.jpg](doc%2Fbzfy.jpg)
![bgsc.jpg](doc%2Fbgsc.jpg)
![bgdy.jpg](doc%2Fbgdy.jpg)
![bgdy2.jpg](doc%2Fbgdy2.jpg)

报告样例下载
[bg.pdf](doc%2Fbg.pdf)


项目启动
mvn compile nutzboot:run mvn运行 或 IDEA 中右击 MainLauncher 运行

mvn package nutzboot:shade -D maven.javadoc.skip=true -D maven.test.skip=true 生成可执行jar包

mvn clean package nutzboot:shade nutzboot:war -D maven.javadoc.skip=true -D maven.test.skip=true 生成可执行war包

正常启动后访问 http://127.0.0.1:8080/sysadmin 
用户名 superadmin 密码 123456

项目部署
内置配置文件启动 nohup java -jar mini.jar & 带参数 -Dnutz.profiles.active=prod(IDEA 运行时填 --nutz.profiles.active=prod) 可加载 application-prod.yaml 文件
外置配置文件启动 nohup java -Dnutz.boot.configure.yaml.dir=/data/budwk/ -jar mini.jar & 此时加载文件夹所有 *.yaml 配置文件
运行环境
JDK 11 + 或 OpenJDK 11 +
Redis 4.0.8 +
MariaDB 10+、MySql 5.7+、Oracle、SqlServer、达梦等
开发工具
IntelliJ IDEA
Maven 3.5.3 +
Git

后期计划：重构整套系统，采用芋道框架+nutz，租户模式。减少冗余代码（开发中，开发时间和经费有限，进度缓慢，敬请期待）

加微信请备注了解lims系统，支持定制开发


<br/>
<img src="doc/wx.jpg" width="300" />





