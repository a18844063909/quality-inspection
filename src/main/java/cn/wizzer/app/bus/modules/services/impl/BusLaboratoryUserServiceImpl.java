package cn.wizzer.app.bus.modules.services.impl;



import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratory;
import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratoryUser;
import cn.wizzer.app.bus.modules.services.BusLaboratoryService;
import cn.wizzer.app.bus.modules.services.BusLaboratoryUserService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;

@IocBean(args = {"refer:dao"})
public class BusLaboratoryUserServiceImpl extends BaseServiceImpl<BusLaboratoryUser> implements BusLaboratoryUserService {

    public BusLaboratoryUserServiceImpl(Dao dao) {
        super(dao);
    }
}
