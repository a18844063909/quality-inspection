package cn.wizzer.app.bus.modules.services.impl;



import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentServicing;
import cn.wizzer.app.bus.modules.services.BusEquipmentServicingService;
import cn.wizzer.app.sys.modules.models.Sys_user;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheRemove;
import org.nutz.plugins.wkcache.annotation.CacheRemoveAll;
import org.nutz.plugins.wkcache.annotation.CacheResult;

@IocBean(args = {"refer:dao"})
public class BusEquipmentServicingServiceImpl extends BaseServiceImpl<BusEquipmentServicing> implements BusEquipmentServicingService {
    public BusEquipmentServicingServiceImpl(Dao dao) {
        super(dao);
    }

    @CacheResult(cacheKey = "${args[0].loginname}_getServicingAuditCount")
    public int getAuditCount(Sys_user user) {
        //待科室主任审核
        int roulSize = this.list(Sqls.create("SELECT d.permission,c.* FROM `sys_role_menu` a  " +
                " left join sys_user_role b on a.roleId = b.roleId " +
                " left join sys_user c on b.userId = c.id " +
                " left join sys_menu d on d.id = a.menuId " +
                " where c.loginname='"+user.getLoginname()+"' and d.permission='equipment.servicing.audit' and c.delflag = 0 and d.delflag = 0 ")).size();
        if(roulSize==0){
            return roulSize;
        }
        String sql ="SELECT count(1) from bus_equipment_servicing es  where es.delFlag=0 and (es.lczt=1 or es.lczt=2) and es.nextsprIds = '"+user.getId()+"'  ";
        int size = this.count(Sqls.create(sql));
        return size;
    }

    @CacheResult(cacheKey = "${args[0].loginname}_getServicingCompleteCount")
    public int getCompleteCount(Sys_user user) {
        int roulSize = this.list(Sqls.create("SELECT d.permission,c.* FROM `sys_role_menu` a  " +
                " left join sys_user_role b on a.roleId = b.roleId " +
                " left join sys_user c on b.userId = c.id " +
                " left join sys_menu d on d.id = a.menuId " +
                " where c.loginname='"+user.getLoginname()+"' and d.permission='equipment.servicing.complete' and c.delflag = 0 and d.delflag = 0 ")).size();
        if(roulSize==0){
            return roulSize;
        }
        String sql ="SELECT count(1) from bus_equipment_servicing es  where es.delFlag=0 and es.lczt=4 and es.opBy = '"+user.getId()+"'  ";;
        int size = this.count(Sqls.create(sql));
        return size;
    }


    @CacheRemove(cacheKey = "${args[0].loginname}_*")
    //可以通过el表达式加 * 通配符来批量删除一批缓存
    public void deleteCache(Sys_user user) {

    }

    @CacheRemoveAll
    public void clearCache() {

    }
}
