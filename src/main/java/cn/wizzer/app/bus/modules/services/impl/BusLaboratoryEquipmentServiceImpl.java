package cn.wizzer.app.bus.modules.services.impl;



import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratoryEquipment;
import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratoryUser;
import cn.wizzer.app.bus.modules.services.BusLaboratoryEquipmentService;
import cn.wizzer.app.bus.modules.services.BusLaboratoryUserService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;

@IocBean(args = {"refer:dao"})
public class BusLaboratoryEquipmentServiceImpl extends BaseServiceImpl<BusLaboratoryEquipment> implements BusLaboratoryEquipmentService {
    public BusLaboratoryEquipmentServiceImpl(Dao dao) {
        super(dao);
    }

}
