package cn.wizzer.app.bus.modules.models;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;
import org.nutz.integration.json4excel.annotation.J4EIgnore;
import org.nutz.integration.json4excel.annotation.J4EName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yxb on 2019/11
 */
@Data
@Table("y_jbxx")
public class YJbxx extends BaseModel implements Serializable {
    private static final long serialVersionUID = 3L;
    @Column
    @Name
    @Comment("ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    @Prev(els = {@EL("uuid()")})
    private String id;


    /*@Column
    @Comment("登记类型")
    @ColDefine(type = ColType.INT, width = 10)
    private Integer djlx;*/

    @Column
    @Comment("检验科室")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String jyks;

    @Column
    @Comment("检验科室编号")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String jyksbh;

    @Column
    @Comment("字号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String zh;

    @Column
    @Comment("样品移交，流转状态")
    @ColDefine(type = ColType.CHAR, width = 32)
    private String lzzt;

    @Column
    @Comment("样品移交是否接收")
    @ColDefine(type = ColType.CHAR, width = 2)
    private String status;

    @Column
    @Comment("样品编号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String ypbh;

    @Column
    @Comment("样品名称")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String ypmc;

    @Column
    @Comment("产品类型")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String yplx;

    @Column
    @Comment("样品等级")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String ypdj;

    @Column
    @Comment("样品状态")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String ypzt;

    @Column
    @Comment("委托方式")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String wtfs;

    @Column
    @Comment("规格型号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String ggxh;

    @Column
    @Comment("所在城市")
    @ColDefine(type = ColType.VARCHAR, width = 106)
    private String szcs;

    @Column
    @Comment("生产日期\\批次\\生产\\加工\\购进日期\\食品批号")
    @ColDefine(type = ColType.VARCHAR, width = 96)
    private String scrqpc;

    @Column
    @Comment("企业联系人")
    @ColDefine(type = ColType.VARCHAR, width = 96)
    private String cyry;
    @Column
    @Comment("抽样人员")
    @ColDefine(type = ColType.VARCHAR, width = 96)
    private String cyryBy;

    @Column
    @Comment("抽样地点")
    @ColDefine(type = ColType.VARCHAR, width = 150)
    private String cydd;

    @Column
    @Comment("抽样日期")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String cyrq;

    @Column
    @Comment("抽样基数")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String cyjs;

    @Column
    @Comment("抽样单位id")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String cydwId;
    @Column
    @Comment("抽样单位")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String cydw;

    @Column
    @Comment("商标")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String sb;

    @Column
    @Comment("检样数量")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jysl;

    @Column
    @Comment("数量单位")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String unit;

    /*@Column
    @Comment("质量等级")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String zldj;*/

    /*@Column
    @Comment("封样状态")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String fyzt;*/

    /*@Column
    @Comment("任务来源")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String rwly;*/

    @Column
    @Comment("抽样单编号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String cydbh;

    @Column
    @Comment("委托单位id")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String wtdwId;
    @Column
    @Comment("委托单位")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String wtdw;

    @Column
    @Comment("委托单位地址")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String wtdwdz;

    @Column
    @Comment("委托单位法人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String wtdwlxr;

    @Column
    @Comment("委托单位手机")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String wtdwsj;

    @Column
    @Comment("委托单位电话")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String wtdwdh;

    @Column
    @Comment("委托单位邮编")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String wtdwyb;

    @Column
    @Comment("受检单位id")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sjdwId;
    @Column
    @Comment("受检单位")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String sjdw;

    @Column
    @Comment("受检单位地址")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String sjdwdz;

    @Column
    @Comment("受检单位法人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sjdwlxr;

    @Column
    @Comment("受检单位手机")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sjdwsj;

    @Column
    @Comment("受检单位电话")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sjdwdh;

    @Column
    @Comment("受检单位邮编")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sjdwyb;

    @Column
    @Comment("生产单位id")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String scdwId;
    @Column
    @Comment("生产单位")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String scdw;

    @Column
    @Comment("生产单位地址")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String scdwdz;

    @Column
    @Comment("受检单位法人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String scdwlxr;

    @Column
    @Comment("生产单位手机")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String scdwsj;

    @Column
    @Comment("生产单位电话")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String scdwdh;

    @Column
    @Comment("生产单位邮编")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String scdwyb;

    /*@Column
    @Comment("工程名称")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String gcmc;

    @Column
    @Comment("工程地址")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String gcdz;

    @Column
    @Comment("工程联系人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String gclxr;

    @Column
    @Comment("工程联系电话")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String gclxdh;

    @Column
    @Comment("施工单位")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String sgdw;

    @Column
    @Comment("设计单位")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String gcsjdw;

    @Column
    @Comment("建设单位")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String jsdw;

    @Column
    @Comment("监理单位")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String jldw;

    @Column
    @Comment("监理人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jlr;

    @Column
    @Comment("见证单位")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String jzdw;

    @Column
    @Comment("见证人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jzr;

    @Column
    @Comment("受理人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String slr;

    @Column
    @Comment("被抽样单位")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String bcydw;

    @Column
    @Comment("被抽样单位地址")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String bcydwdz;

    @Column
    @Comment("被抽样单位联系人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bcydwlxr;

    @Column
    @Comment("被抽样单位手机")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bcydwsj;

    @Column
    @Comment("被抽样单位电话")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bcydwdh;

    @Column
    @Comment("被抽样单位邮编")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bcydwyb;

    @Column
    @Comment("标示生产者")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String bsscz;

    @Column
    @Comment("标示生产地址")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String bsscdz;

    @Column
    @Comment("标示生产联系人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bssclxr;

    @Column
    @Comment("标示生产手机")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bsscsj;

    @Column
    @Comment("标示生产电话")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bsscdh;

    @Column
    @Comment("标示生产邮编")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bsscyb;*/

    @Column
    @Comment("检验项目")
    @ColDefine(type = ColType.VARCHAR, width = 500)
    private String jyxm;

    @Column
    @Comment("检验依据")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String jyyj;

    @Column
    @Comment("检验费用")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jyfy;

    @Column
    @Comment("检验费用状态")
    @ColDefine(type = ColType.VARCHAR, width = 20)
    private String jyfydd;

    @Column
    @Comment("验后是否需退库")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String yhxtk;

    @Column
    @Comment("检验类别")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jylx;

    @Column
    @Comment("检验合同号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jyhth;

    @Column
    @Comment("检查封样人员")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String jcfyry;

    @Column
    @Comment("报告发送方式")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bgfsfs;

    @Column
    @Comment("到样日期")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String dyrq;

    @Column
    @Comment("完成期限")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String wcqx;

    @Column
    @Comment("样品附件")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String fj;

    @Column
    @Comment("备注")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String bz;

    @Column
    @Comment("特殊说明")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String tssm;

    /*@Column
    @Comment("样品修改人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String xgr;

    @Column
    @Comment("样品修改时间")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String xgsj;*/

    @Column
    @Comment("流程状态")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String lczt;

    @Column
    @Comment("是否退样")
    @ColDefine(type = ColType.INT, width = 32)
    private String sfty;

    @Column
    @Comment("退样状态")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String tyzt;

    @Column
    @Comment("接收人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jsr;

    /*@Column
    @Comment("接收时间")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jssj;*/

    /*@Column
    @Comment("移交状态")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private Integer yjzt;*/

    @Column
    @Readonly
    private String yjr;

    @Column
    @Comment("移交时间")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String yjsj;

    @Column
    @Comment("移交数量")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String yjsl;
    @Column
    @Comment("退样数量")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String tysl;
    @Column
    @Comment("归还数量")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String ghsl;

    /*@Column
    @Comment("移交备注")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String yjbz;*/

   /* @Column
    @Comment("领用数量")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String lysl;

    @Column
    @Comment("所属编号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String ssbh;

    @Column
    @Comment("办理人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String blr;

    @Column
    @Comment("登记日期")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String djrq;

    @Column
    @Comment("领样人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String lyr;

    @Column
    @Comment("领样日期")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String lyrq;

    @Column
    @Comment("手机号码")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sjhm;
*/
    /*@Column
    @Comment("有无备样")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private Integer ywby;*/

    @Column
    @Comment("检验日期")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jyrq;

    @Column
    @Readonly
    private String bzr;

    @Column
    @Comment("检验结论")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String jyjl;

    @Column
    @Comment("检验情况说明")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String jyqksm;

    /*@Column
    @Comment("认证方式")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String rzfs;*/

    /*@Column
    @Comment("是否为特殊报告")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String tsbg;*/

    /*@Column
    @Comment("单位名称图片")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String dwmctp;*/

    /*@Column
    @Comment("解锁备注")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jsbz;*/

   /* @Column
    @Comment("档案类型")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String dalx;

    @Column
    @Comment("档案关键字")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String dagjz;

    @Column
    @Comment("所属类目ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sslmid;

    @Column
    @Comment("所属类目名称")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String lmmc;

    @Column
    @Comment("归档附件")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String gdfj;

    @Column
    @Comment("归档内容")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String gdnr;

    @Column
    @Comment("档案密级")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String damj;

    @Column
    @Comment("保管期限")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String bgqx;

    @Column
    @Comment("可查看人ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String kckrid;

    @Column
    @Comment("可查看人姓名")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String kckrxm;

    @Column
    @Comment("是否清档")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sfqd;

    @Column
    @Comment("有无原始记录")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String ywysjl;

    @Column
    @Comment("原始记录文件名")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String ysjlwjm;

    @Column
    @Comment("原始记录随机名")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String ysjlsjm;

    @Column
    @Comment("原始记录")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String ysjl;

    @Column
    @Comment("上传人员")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String scry;

    @Column
    @Comment("上传时间")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String scsj;

    @Column
    @Comment("归档备注")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String gdbz;

    @Column
    @Comment("归档人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String gdr;

    @Column
    @Comment("归档时间")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String gdsj;*/

    @Column
    @Comment("是否延期")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sfyq;

    @Column
    @Comment("延期日期")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String yqrq;

    /*@Column
    @Comment("延期人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String yqr;*/

    /*@Column
    @Comment("延期操作时间")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String yqsj;*/

    @Column
    @Comment("打印状态")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String dyzt;

    @Column
    @Comment("打印次数")
    @ColDefine(type = ColType.CHAR, width = 3)
    private String dycs;

    @Column
    @Comment("打印时间")
    @ColDefine(type = ColType.VARCHAR, width = 40)
    private String dysj;

    @Column
    @Comment("收费状态")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sfzt;

    @Column
    @Comment("已交金额")
    @ColDefine(type = ColType.VARCHAR, width = 20)
    private String yjje;

    @Column
    @Comment("剩缴金额")
    @ColDefine(type = ColType.VARCHAR, width = 20)
    private String sjje;

    @Column
    @Comment("附件名称")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String fjmc;

    /*@Column
    @Comment("报告打印时间")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bgdysj;*/

    @Column
    @Comment("二维码编号")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String ewmbh;

    /*@Column
    @Comment("批准章名称图片")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String pzzmctp;	*/	//批准章名称图片

    /*@Column
    @Comment("打印时间（保存最新一条）")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String dysj;*/

    /*@Column
    @Comment("商标路径")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String sblj;*/		//商标文件的路径

    @Column
    @Comment("备样数量")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bysl;

    @Column
    @Comment("企业联系方式")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String jyfdb;



    @Column
    @Comment("有效期/保质期")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String validity;


    @Column
    @Comment("提供的资料")
    @ColDefine(type = ColType.VARCHAR, width = 300)
    private String information;


    @Column
    @Comment("样品贮存")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String storage;


    @Column
    @Comment("包装")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String packing;


    @Column
    @Comment("分包检验项目/分包方名称")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String subcontract;


    @Column
    @Comment("客户意向")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String intention;


    @Column
    @Comment("样品外观")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String exterior;
    @Column
    @Comment("协议书备注")
    @ColDefine(type = ColType.VARCHAR, width = 455)
    private String xysRemark;
    @Column
    @Comment("检验时限")
    @ColDefine(type = ColType.INT, width = 3)
    private String limitDay;


    @Column
    @Comment("检验完成日期")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jywcrq;

    @Column
    @Comment("试毕样品处理情况")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String byczqk;

    @Column
    @Comment("报告编号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bgbh;
    @Column
    @Comment("检验结果")
    @ColDefine(type = ColType.CHAR, width = 1)
    private String jyjg;


    /**
     * 检验项目
     */
    @Many(field = "jbxxId")
    private List<YJbxxJyxm> yjbxxjyxm;


    //excel导出增加字段
    @Column
    @Readonly
    private String djr;
    @Column
    @Readonly
    private String djsj;
    @Column
    @Readonly
    private String jyry;
    @Column
    @Readonly
    private String bzsj;
    @Column
    @Readonly
    private String shr;
    @Column
    @Readonly
    private String shsj;
    @Column
    @Readonly
    private String pzr;
    @Column
    @Readonly
    private String pzsj;
    @Column
    @Readonly
    private String tyr;
    @Column
    @Readonly
    private String tysj;
    @Column
    @Readonly
    private String ghr;
    @Column
    @Readonly
    private String ghsj;

}
