package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.InspectionItem;
import cn.wizzer.framework.base.service.BaseService;

/**
 * 检验项目
 */
public interface InspectionItemService extends BaseService<InspectionItem> {
    /**
     * 清除一个实体缓存
     * @param id
     */
    void deleteCache(String id);

    /**
     * 清空缓存
     */
    void clearCache();

}
