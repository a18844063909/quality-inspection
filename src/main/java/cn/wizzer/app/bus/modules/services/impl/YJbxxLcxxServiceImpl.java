package cn.wizzer.app.bus.modules.services.impl;

import cn.wizzer.app.bus.modules.models.YJbxxLcxx;
import cn.wizzer.app.bus.modules.services.YJbxxLcxxService;
import cn.wizzer.app.sys.modules.models.Sys_unit;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.lang.util.NutMap;
import org.nutz.plugins.wkcache.annotation.CacheDefaults;
import org.nutz.plugins.wkcache.annotation.CacheRemove;
import org.nutz.plugins.wkcache.annotation.CacheRemoveAll;
import org.nutz.plugins.wkcache.annotation.CacheResult;

import java.util.List;
import java.util.Map;


/**
 * Created by wizzer on 2016/12/22.
 */
@IocBean(args = {"refer:dao"})
@CacheDefaults(cacheName = "y_jbxx_lcxx")
public class YJbxxLcxxServiceImpl extends BaseServiceImpl<YJbxxLcxx> implements YJbxxLcxxService {
    public YJbxxLcxxServiceImpl(Dao dao) {
        super(dao);
    }
    @CacheResult(cacheKey = "${args[0]}_getTaskCount")
    public int getTaskCount(String loginname) {
        //待分配任务
        //查询是否有权限
        int roulSize = this.list(Sqls.create("SELECT d.permission,c.* FROM `sys_role_menu` a  " +
                " left join sys_user_role b on a.roleId = b.roleId " +
                " left join sys_user c on b.userId = c.id " +
                " left join sys_menu d on d.id = a.menuId " +
                " where c.loginname='"+loginname+"' and d.permission='bus.rwgl.rwfp' and c.delflag = 0 and d.delflag = 0 ")).size();
        if(roulSize==0){
            return roulSize;
        }
        String sql ="SELECT count(1) FROM y_jbxx jbxx LEFT JOIN ( SELECT jsfjl.ypbh, sum(jsfjl.bcss) AS ysfy, max(jsfjl.jfrq) AS jfrq FROM Y_Jbxx_sfjl jsfjl GROUP BY jsfjl.ypbh ) sfjl ON jbxx.ypbh = sfjl.ypbh where 1=1 and jbxx.lczt in(1,3) and jbxx.lzzt>0 and jbxx.delflag = 0 ";
        NutMap map = this.getNutMap(Sqls.create("select 'id',b.id from sys_user a " +
                " left join sys_unit b on a.unitid = b.id " +
                " where a.loginname='"+loginname+"' and b.sfjybm=1 and a.delFlag=0 and b.delflag = 0 "));
        if(map.size()>0){
            sql+= " and jbxx.jyksbh like '%"+map.get("id")+"%' ";
        }
        int size = this.count(Sqls.create(sql));
        return size;
    }
    @CacheResult(cacheKey = "${args[0]}_getWorkCount")
    public int getWorkCount(String loginname) {
        //待编制报告
        int roulSize = this.list(Sqls.create("SELECT d.permission,c.* FROM `sys_role_menu` a  " +
                " left join sys_user_role b on a.roleId = b.roleId " +
                " left join sys_user c on b.userId = c.id " +
                " left join sys_menu d on d.id = a.menuId " +
                " where c.loginname='"+loginname+"' and d.permission='bus.bggl.bgbz'")).size();
        if(roulSize==0){
            return roulSize;
        }
        String lcxxSql =" and nextsprIds like concat('%',(select id from sys_user where loginname='"+loginname+"'),'%')";
        String sql ="SELECT count(1) FROM y_jbxx jbxx  where 1=1 and jbxx.lczt in (2,4,6,8,12) and jbxx.delflag = 0 and id in (select jbxxid from  y_jbxx_lcxx where lczt in(2,4,6,8,12)  and delflag=0 "+lcxxSql+")";

        int size = this.count(Sqls.create(sql));
        return size;
    }

    @CacheResult(cacheKey = "${args[0]}_getExamineCount")
    public int getExamineCount(String loginname) {
        //待审核报告

        int roulSize = this.list(Sqls.create("SELECT d.permission,c.* FROM `sys_role_menu` a  " +
                " left join sys_user_role b on a.roleId = b.roleId " +
                " left join sys_user c on b.userId = c.id " +
                " left join sys_menu d on d.id = a.menuId " +
                " where c.loginname='"+loginname+"' and d.permission='bus.bggl.bgsh'")).size();
        if(roulSize==0){
            return roulSize;
        }
        String lcxxSql =" and nextsprIds like concat('%',(select id from sys_user where loginname='"+loginname+"'),'%')";
        String sql ="SELECT count(1) FROM y_jbxx jbxx  where 1=1 and jbxx.lczt =5 and jbxx.delflag = 0 and id in (select jbxxid from  y_jbxx_lcxx where lczt =5  and delflag=0 "+lcxxSql+")";
        int size = this.count(Sqls.create(sql));
        return size;
    }
    @CacheResult(cacheKey = "${args[0]}_getApproveCount")
    public int getApproveCount(String loginname) {
        //待批准报告

        int roulSize = this.list(Sqls.create("SELECT d.permission,c.* FROM `sys_role_menu` a  " +
                " left join sys_user_role b on a.roleId = b.roleId " +
                " left join sys_user c on b.userId = c.id " +
                " left join sys_menu d on d.id = a.menuId " +
                " where c.loginname='"+loginname+"' and d.permission='bus.bggl.bgpz'")).size();
        if(roulSize==0){
            return roulSize;
        }
        String lcxxSql =" and nextsprIds like concat('%',(select id from sys_user where loginname='"+loginname+"'),'%')";
        String sql ="SELECT count(1) FROM y_jbxx jbxx  where 1=1 and jbxx.lczt =7 and jbxx.delflag = 0 and id in (select jbxxid from  y_jbxx_lcxx where lczt =7  and delflag=0 "+lcxxSql+")";
        int size = this.count(Sqls.create(sql));
        return size;
    }
    @CacheResult(cacheKey = "${args[0]}_getPrintCount")
    public int getPrintCount(String loginname) {
        //待打印报告
        int roulSize = this.list(Sqls.create("SELECT d.permission,c.* FROM `sys_role_menu` a  " +
                " left join sys_user_role b on a.roleId = b.roleId " +
                " left join sys_user c on b.userId = c.id " +
                " left join sys_menu d on d.id = a.menuId " +
                " where c.loginname='"+loginname+"' and d.permission='bus.bggl.bgdy'")).size();
        if(roulSize==0){
            return roulSize;
        }
        String sql =" SELECT count(1) FROM y_jbxx jbxx " +
                " left join sys_user b on jbxx.opBy=b.id " +
                " where jbxx.lczt in (9,10,11) and jbxx.delflag = 0 and b.delFlag=0 " +
                " and b.loginname = '"+loginname+"' " +
                " and jbxx.dyzt!=1";
        int size = this.count(Sqls.create(sql));
        return size;
    }
    @CacheRemove(cacheKey = "${args[0]}_*")
    //可以通过el表达式加 * 通配符来批量删除一批缓存
    public void deleteCache(String loginname) {

    }

    @CacheRemoveAll
    public void clearCache() {

    }

}
