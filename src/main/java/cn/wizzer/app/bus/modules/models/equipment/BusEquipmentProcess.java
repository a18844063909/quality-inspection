package cn.wizzer.app.bus.modules.models.equipment;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;

/**
* @author tcjinxiu.com
* @time   2024-04-11 10:19:44
*/
@Data
@Table("bus_equipment_process")
public class BusEquipmentProcess extends BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column
	@Name
	@Prev(els = {@EL("uuid()")})
	private String id;
	@Column
	private String headId;
	@Column
	private String equipmentId;
	@Column
	private String type;
	@Column
	private String lczt;
	@Column
	private String status;
	@Column
	private String opinion;
	@Column
	private String nextsprIds;
	@Column
	private String nextspr;

}
