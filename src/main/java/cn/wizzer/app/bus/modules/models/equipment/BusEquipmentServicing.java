package cn.wizzer.app.bus.modules.models.equipment;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;
import org.nutz.dao.entity.annotation.Name;
/**
* @author tcjinxiu.com
* @time   2024-04-11 10:19:44
*/
@Data
@Table("bus_equipment_servicing")
public class BusEquipmentServicing extends BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column
	@Name
	@Prev(els = {@EL("uuid()")})
	private String id;
	@Column
	private String equipmentId;
	@Column
	private String lczt;
	@Column
	private String symptom;
	@Column
	private String calibrationFlag;
	@Column
	private String repairDate;
	@Column
	private String repairRemak;
	@Column
	private String repairContent;
	@Column
	private String cause;
	@Column
	private String repairUnit;
	@Column
	private String repairPrice;
	@Column
	private String returnDate;
	@Column
	private String acceptanceBy;
	@Column
	private String acceptanceRemak;
	@Column
	private String fileId;
	@Column
	private String auditFlag;
	@Column
	private String nextsprIds;
	@Column
	private String nextspr;

}
