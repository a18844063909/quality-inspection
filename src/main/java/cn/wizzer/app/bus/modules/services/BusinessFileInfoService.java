package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.BusinessFileInfo;
import cn.wizzer.framework.base.service.BaseService;

/**
 * 模板
 */
public interface BusinessFileInfoService extends BaseService<BusinessFileInfo> {


    /**
     * 清除一个实体缓存
     * @param reportTemplateId
     */
    void deleteCache(String reportTemplateId);

    /**
     * 清空缓存
     */
    void clearCache();

}
