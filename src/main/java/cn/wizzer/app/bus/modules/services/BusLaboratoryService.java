package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.equipment.BusEquipment;
import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratory;
import cn.wizzer.framework.base.service.BaseService;

/**
 * @author tcjinxiu.com
 * @time 2024-04-11 10:19:44
 *
 */
public interface BusLaboratoryService extends BaseService<BusLaboratory> {
}
