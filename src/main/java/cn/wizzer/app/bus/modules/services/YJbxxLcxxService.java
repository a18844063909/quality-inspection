package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.YJbxxLcxx;
import cn.wizzer.framework.base.service.BaseService;

/**
 * Created by yx on 2019
 */
public interface YJbxxLcxxService extends BaseService<YJbxxLcxx> {
    int getTaskCount(String loginname);
    int getWorkCount(String loginname);
    int getExamineCount(String loginname);
    int getApproveCount(String loginname);
    int getPrintCount(String loginname);
    /**
     * 清空缓存
     */
    void clearCache();

}
