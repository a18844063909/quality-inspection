package cn.wizzer.app.bus.modules.services.impl;



import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentProcess;
import cn.wizzer.app.bus.modules.services.BusEquipmentProcessService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.util.NutMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@IocBean(args = {"refer:dao"})
public class BusEquipmentProcessServiceImpl extends BaseServiceImpl<BusEquipmentProcess> implements BusEquipmentProcessService {
    public BusEquipmentProcessServiceImpl(Dao dao) {
        super(dao);
    }
    public List<NutMap> listThisProcess(Map all ,int[] process){
        List<NutMap> listThisProcess = new ArrayList<>();
        for (int p : process){
            NutMap map = NutMap.NEW().addv("value", p).addv("label", all.get(p+""));
            listThisProcess.add(map);
        }
        return listThisProcess;
    }

    /*
    * processType 流程类型
    * process 当前流程
    * status 审核状态
    * return  下一个流程
    * */
    public int getNextProcess(int[] processType, int process, int status){
        int nextProcess = 0;
        if(status==0){
            return process;
        }
        for(int i =0; i<processType.length;i++){
            int p = processType[i];
            if(p ==  process){
                if(status==-1){
                    if(process==2){
                        nextProcess = processType[i-1];
                    }else if(process == 10){
                        if(processType.length>4) {
                            nextProcess = processType[i-2];
                        }else{
                            nextProcess = processType[i-1];
                        }
                    }else{
                        nextProcess = processType[i-2];

                    }
                }else{
                    nextProcess = processType[i+1];
                }
                break;
            }
        }
        return nextProcess;
    }

    /*
     * processType 流程类型
     * process 当前流程
     *
     *
     * return 可查询的流程范围
     *
     * */
    public String getQueryProcess(int[] processType, String process){
        String stringProcess = "";
        if(process.equals("0")){
            return Arrays.toString(processType).replace("[", "").replace("]", "");
        }
        for(int i =0; i<processType.length;i++){
            int p = processType[i];
            if(process.equals(p+"")){
                if(process.equals("2")){
                    stringProcess = processType[i-1]+","+processType[i]+","+processType[i+1];
                }else if(process.equals("10")){
                    if(processType.length>4) {
                        stringProcess = processType[i - 2] + "," + processType[i];
                    }else{
                        stringProcess = processType[i - 1] + "," + processType[i];
                    }
                }else{
                    stringProcess = processType[i-2]+","+processType[i]+","+processType[i+1];
                }
                break;
            }
        }
        if(StringUtils.isNotBlank(stringProcess)){
            return stringProcess;
        }else{
            return process;
        }
    }
    /*
     * processType 流程类型
     * process 当前流程
     *
     *
     * return 可操作的流程范围
     *
     * */
    public String getOperationProcess(int[] processType, String process){
        String stringProcess = "";
        if(process.equals("0")){
            stringProcess= "0";
        }
        for(int i =0; i<processType.length;i++){
            if(process.equals("0")){
                if (i % 2 == 1&&i>1) {
                    stringProcess+=","+processType[i];
                }
            }else {
                int p = processType[i];
                if (process.equals(p + "")) {
                    if(process.equals("2")){
                        stringProcess = processType[i-1]+","+processType[i]+","+processType[i+1];
                    }else if(process.equals("10")){
                        if(processType.length>4) {
                            stringProcess = processType[i - 2] + "," + processType[i];
                        }else{
                            stringProcess = processType[i - 1] + "," + processType[i];
                        }
                    }else{
                        stringProcess = processType[i-2]+","+processType[i]+","+processType[i+1];
                    }
                    break;
                }
            }
        }
        if(StringUtils.isNotBlank(stringProcess)){
            return stringProcess;
        }else{
            return process;
        }
    }
}
