package cn.wizzer.app.bus.modules.models.equipment;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.beans.Transient;
import java.io.Serializable;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.integration.json4excel.annotation.J4EIgnore;
import org.nutz.integration.json4excel.annotation.J4EName;

/**
* @author tcjinxiu.com
* @time   2024-04-11 10:19:44
*/
@Data
@J4EName("设备台账")
@Table("bus_equipment")
public class BusEquipment extends BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column
	@Name
	@Comment("ID")
	@J4EIgnore
	@ColDefine(type = ColType.VARCHAR, width = 32)
	@Prev(els = {@EL("uuid()")})
	private String id;
	@Column
	@J4EName("设备编号")
	@Comment("设备编号")
	private String code;
	@Column
	@J4EName("设备编号")
	@Comment("设备编号")
	private String name;
	@Column
	@J4EName("规格型号")
	@Comment("规格型号")
	private String model;
	@Column
	@J4EName("检测范围")
	@Comment("检测范围")
	private String detectionRange;
	@Column
	@J4EName("精度要求")
	@Comment("精度要求")
	private String accuracy;
	@Column
	@J4EName("出厂编号")
	@Comment("出厂编号")
	private String factoryCode;
	@Column
	@J4EName("数量（台/套）")
	@Comment("数量（台/套）")
	private Integer number;
	@Column
	@J4EName("使用状态")
	@Comment("使用状态")
	private String status;
	@Column
	@J4EName("制造厂商")
	@Comment("制造厂商")
	private String company;
	@Column
	@J4EName("购置价格（元）")
	@Comment("购置价格（元）")
	private String purchasePrice;
	@Column
	@J4EName("购置日期")
	@Comment("购置日期")
	private String purchaseDate;
	@Column
	@J4EName("计量情况")
	@Comment("计量情况")
	private String calculate;
	@Column
	@J4EName("计量单位")
	@Comment("计量单位")
	private String calculateUnit;
	@Column
	@J4EName("计量周期")
	@Comment("计量周期")
	private String calculateCycle;
	@Column
	@J4EName("计量周期类型")
	@Comment("计量周期类型")
	private String calculateCycleType;
	@Column
	@J4EName("下次计量日期")
	@Comment("下次计量日期")
	private String calculateDate;
	@Column
	@J4EName("证书编号")
	@Comment("证书编号")
	private String reportCode;
	@Column
	@J4EName("存放地点")
	@Comment("存放地点")
	private String depositAdress;
	@Column
	@J4EIgnore
	@Comment("设备责任人")
	private String managerBy;
	@Column
	@J4EIgnore
	@Comment("使用部门")
	private String applyDept;
	@Column
	@J4EName("分类情况")
	@Comment("分类情况")
	private String classification;
	@Column
	@J4EName("是否进口设备")
	@Comment("是否进口设备")
	private String importedFlag;
	@Column
	@J4EName("资产编号")
	@Comment("资产编号")
	private String assetCode;
	@Column
	@J4EName("供货商联系电话")
	@Comment("供货商联系电话")
	private String supplyPhone;
	@Column
	@J4EName("备注")
	@Comment("备注")
	private String remark;
	@Column
	@J4EName("设备责任人")
	@Readonly
	private String managerName;
	@Column
	@J4EName("使用部门")
	@Readonly
	private String applyName;

}
