package cn.wizzer.app.bus.modules.models.equipment;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;
import org.nutz.dao.entity.annotation.Name;
/**
* @author tcjinxiu.com
* @time   2024-04-11 10:19:44
*/
@Data
@Table("bus_equipment_inspection_inventory")
public class BusEquipmentInspectionInventory extends BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column
	@Name
	@Prev(els = {@EL("uuid()")})
	private String id;
	@Column
	private String inspectionId;
	@Column
	private String equipmentId;
	@Column
	private String inspectionInId;
	@Column
	private String outStatus;
	@Column
	private String exterior;
	@Column
	private String inStatus;
	@Column
	private int sort;

	@Column
	private int inSort;

	private String equipmentName;
	private String equipmentCode;

	/*@One(field = "inspectionId",key = "id")
	private BusEquipment equipment;*/

}
