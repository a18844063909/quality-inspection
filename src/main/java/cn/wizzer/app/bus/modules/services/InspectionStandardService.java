package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.InspectionItem;
import cn.wizzer.app.bus.modules.models.InspectionStandard;
import cn.wizzer.framework.base.service.BaseService;

import java.util.List;

/**
 * 检验标准
 */
public interface InspectionStandardService extends BaseService<InspectionStandard> {

    /**
     * 清除一个实体缓存
     * @param inspectionStandardId
     */
    void deleteCache(String inspectionStandardId);

    /**
     * 清空缓存
     */
    void clearCache();

}
