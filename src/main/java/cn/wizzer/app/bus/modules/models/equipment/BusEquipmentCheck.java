package cn.wizzer.app.bus.modules.models.equipment;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;
import org.nutz.dao.entity.annotation.Name;
/**
* @author tcjinxiu.com
* @time   2024-04-11 10:19:44
*/
@Data
@Table("bus_equipment_check")
public class BusEquipmentCheck extends BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column
	@Name
	@Prev(els = {@EL("uuid()")})
	private String id;
	@Column
	private String equipmentId;
	@Column
	private String lczt;
	@Column
	private String checkTime;
	/*@Column
	private String inspector;*/
	@Column
	private String checkStatus;
	@Column
	private String cause;
	@Column
	private String maintenance;
	@Column
	private String servicing;
	@Column
	private String servicingId;
	@Column
	private String fileId;

	@Column
	private String auditFlag;
	@Column
	private String nextsprIds;
	@Column
	private String nextspr;
}
