package cn.wizzer.app.bus.modules.services.impl;

import cn.wizzer.app.bus.modules.models.YJbxx;
import cn.wizzer.app.bus.modules.services.YJbxxService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheDefaults;


/**
 * Created by wizzer on 2016/12/22.
 */
@IocBean(args = {"refer:dao"})
@CacheDefaults(cacheName = "y_jbxx")
public class YJbxxServiceImpl extends BaseServiceImpl<YJbxx> implements YJbxxService {
    public YJbxxServiceImpl(Dao dao) {
        super(dao);
    }


}
