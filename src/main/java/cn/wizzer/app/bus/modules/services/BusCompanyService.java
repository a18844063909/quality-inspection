package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.BusCompany;
import cn.wizzer.app.bus.modules.models.BusinessFileInfo;
import cn.wizzer.framework.base.service.BaseService;

/**
 * 模板
 */
public interface BusCompanyService extends BaseService<BusCompany> {

}
