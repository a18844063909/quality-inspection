package cn.wizzer.app.bus.modules.models;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;

@Data
@Table("bus_character")
public class BusCharacter extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column
    @Name
    @Comment("ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    @Prev(els = {@EL("uuid()")})
    private String id;

    @Column
    @Comment("字符")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String txt;

    @Column
    @Comment("备注")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String remark;
}
