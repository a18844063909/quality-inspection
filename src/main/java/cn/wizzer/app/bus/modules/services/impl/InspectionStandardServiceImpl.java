package cn.wizzer.app.bus.modules.services.impl;

import cn.wizzer.app.bus.modules.models.InspectionStandard;
import cn.wizzer.app.bus.modules.services.InspectionItemService;
import cn.wizzer.app.bus.modules.services.InspectionStandardService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheRemove;
import org.nutz.plugins.wkcache.annotation.CacheRemoveAll;

import java.util.List;

@IocBean(args = {"refer:dao"})
public class InspectionStandardServiceImpl extends BaseServiceImpl<InspectionStandard> implements InspectionStandardService {

    public InspectionStandardServiceImpl(Dao dao) {
        super(dao);
    }

    @Inject
    private InspectionItemService inspectionItemService;



    @CacheRemoveAll
    public void clearCache() {
    }


    @CacheRemove(cacheKey = "${args[0]}_*")
    //可以通过el表达式加 * 通配符来批量删除一批缓存
    public void deleteCache(String inspectionStandardId) {
    }

}
