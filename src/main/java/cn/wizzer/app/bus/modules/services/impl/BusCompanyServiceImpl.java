package cn.wizzer.app.bus.modules.services.impl;

import cn.wizzer.app.bus.modules.models.BusCompany;
import cn.wizzer.app.bus.modules.models.BusinessFileInfo;
import cn.wizzer.app.bus.modules.services.BusCompanyService;
import cn.wizzer.app.bus.modules.services.BusinessFileInfoService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheRemove;
import org.nutz.plugins.wkcache.annotation.CacheRemoveAll;

@IocBean(args = {"refer:dao"})
public class BusCompanyServiceImpl extends BaseServiceImpl<BusCompany> implements BusCompanyService {
    public BusCompanyServiceImpl(Dao dao) {
        super(dao);
    }

}
