package cn.wizzer.app.bus.modules.models;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;

@Data
@Table("bus_file_info")
public class BusinessFileInfo extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column
    @Name
    @Comment("ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    @Prev(els = {@EL("uuid()")})
    private String id;

    @Column
    @Comment("名称")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String infoName;

    @Column
    @Comment("版本号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String infoVersion;

    @Column
    @Comment("产品类别")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String productType;

    @Column
    @Comment("类别")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String infoType;

    @Column
    @Comment("文件类型")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String fileType;

    @Column
    @Comment("存储的路径")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String filePath;

    @Column
    @Comment("文件大小")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private Long fileSize;

    @Column
    @Comment("随机生成的文件名")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String fileName;

    @Column
    @Comment("原来的文件名")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String fileBeforeName;

    @Column
    @Comment("存储绝对URL")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String fileUrl;

    @Column
    @Comment("是否禁用")
    @ColDefine(type = ColType.BOOLEAN)
    private boolean disabled;


    @Column
    @Comment("操作人姓名")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String operatorName;

    @Column
    @Comment("备注")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String remark;

}
