package cn.wizzer.app.bus.modules.services.impl;

import cn.wizzer.app.bus.modules.models.BusCharacter;
import cn.wizzer.app.bus.modules.models.BusCompany;
import cn.wizzer.app.bus.modules.services.BusCharacterService;
import cn.wizzer.app.bus.modules.services.BusCompanyService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;

@IocBean(args = {"refer:dao"})
public class BusCharacterServiceImpl extends BaseServiceImpl<BusCharacter> implements BusCharacterService {
    public BusCharacterServiceImpl(Dao dao) {
        super(dao);
    }

}
