package cn.wizzer.app.bus.modules.services.impl;

import cn.wizzer.app.bus.modules.models.YJbxxJyxm;
import cn.wizzer.app.bus.modules.services.YJbxxJyxmService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheDefaults;


/**
 * Created by wizzer on 2016/12/22.
 */
@IocBean(args = {"refer:dao"})
@CacheDefaults(cacheName = "Y_Jbxx_Jyxm")
public class YJbxxJyxmServiceImpl extends BaseServiceImpl<YJbxxJyxm> implements YJbxxJyxmService {
    public YJbxxJyxmServiceImpl(Dao dao) {
        super(dao);
    }


}
