package cn.wizzer.app.bus.modules.models.equipment;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;
import java.util.List;

import org.nutz.dao.entity.annotation.Name;
/**
* @author tcjinxiu.com
* @time   2024-04-11 10:19:44
*/
@Data
@Table("bus_equipment_report")
public class BusEquipmentReport extends BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column
	@Name
	@Prev(els = {@EL("uuid()")})
	private String id;
	@Column
	private String equipmentId;
	@Column
	private String inspectionId;
	@Column
	private String calculateDate;
	@Column
	private String lczt;
	@Column
	private String calculate;
	@Column
	private String explains;
	@Column
	private String reportCode;
	@Column
	private String calculateAccord;

	@Column
	private String ability;
	@Column
	private String accord;
	@Column
	private String conclusion;
	@Column
	private String fileId;
	@Column
	private String auditFlag;
	@Column
	private String nextsprIds;
	@Column
	private String nextspr;
	@Column
	private String remark;

	@Many(field = "reportId")
	private List<BusEquipmentReportItem> reportItems;

}
