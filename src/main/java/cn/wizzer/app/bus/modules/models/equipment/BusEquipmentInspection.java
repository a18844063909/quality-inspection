package cn.wizzer.app.bus.modules.models.equipment;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;
import java.util.List;

import org.nutz.dao.entity.annotation.Name;
/**
* @author tcjinxiu.com
* @time   2024-04-11 10:19:44
*/
@Data
@Table("bus_equipment_inspection")
public class BusEquipmentInspection extends BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column
	@Name
	@Prev(els = {@EL("uuid()")})
	private String id;
	@Column
	private String type;
	@Column
	private String lczt;
	@Column
	private String measurementUnit;
	@Column
	private String outFileIds;
	@Column
	private String outMeBy;
	@Column
	private String outAt;
	@Column
	private String inFileIds;
	@Column
	private String calculateDate;
	@Column
	private String inAt;
	@Column
	private String inMeBy;
	@Column
	private String outRemark;
	@Column
	private String inRemark;
	@Column
	private String auditFlag;
	@Column
	private String nextsprIds;
	@Column
	private String nextspr;

	@Many(field = "inspectionId")
	private List<BusEquipmentInspectionInventory> inventorys;

	@Many(field = "inspectionInId")
	private List<BusEquipmentInspectionInventory> inventoryIns;


}
