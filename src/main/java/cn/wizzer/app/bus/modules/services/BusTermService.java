package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.BusCompany;
import cn.wizzer.app.bus.modules.models.BusTerm;
import cn.wizzer.framework.base.service.BaseService;

/**
 * 模板
 */
public interface BusTermService extends BaseService<BusTerm> {

}
