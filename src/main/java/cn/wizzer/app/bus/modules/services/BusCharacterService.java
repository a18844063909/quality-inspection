package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.BusCharacter;
import cn.wizzer.framework.base.service.BaseService;

/**
 * 模板
 */
public interface BusCharacterService extends BaseService<BusCharacter> {

}
