package cn.wizzer.app.bus.modules.models;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;

@Data
@Table("bus_company")
public class BusCompany extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column
    @Name
    @Comment("ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    @Prev(els = {@EL("uuid()")})
    private String id;

    @Column
    @Comment("名称")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String name;

    @Column
    @Comment("地址")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String address;

    @Column
    @Comment("联系人")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String contacts;

    @Column
    @Comment("手机")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String phone;

    @Column
    @Comment("电话")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String telephone;

    @Column
    @Comment("邮编")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String postalCode;

    @Column
    @Comment("备注")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String remark;
}
