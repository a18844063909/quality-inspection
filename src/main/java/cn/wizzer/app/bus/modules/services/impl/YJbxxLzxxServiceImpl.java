package cn.wizzer.app.bus.modules.services.impl;

import cn.wizzer.app.bus.modules.models.YJbxxLcxx;
import cn.wizzer.app.bus.modules.models.YJbxxLzxx;
import cn.wizzer.app.bus.modules.services.YJbxxLcxxService;
import cn.wizzer.app.bus.modules.services.YJbxxLzxxService;
import cn.wizzer.app.sys.modules.models.Sys_msg_user;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.dao.pager.Pager;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheDefaults;
import org.nutz.plugins.wkcache.annotation.CacheRemove;
import org.nutz.plugins.wkcache.annotation.CacheRemoveAll;
import org.nutz.plugins.wkcache.annotation.CacheResult;

import java.util.List;


/**
 * Created by wizzer on 2016/12/22.
 */
@IocBean(args = {"refer:dao"})
@CacheDefaults(cacheName = "y_jbxx_lzxx")
public class YJbxxLzxxServiceImpl extends BaseServiceImpl<YJbxxLzxx> implements YJbxxLzxxService {
    public YJbxxLzxxServiceImpl(Dao dao) {
        super(dao);
    }

    @CacheResult(cacheKey = "${args[0]}_getTransferredCount")
    public int getTransferredCount(String loginname) {
        //待移交数量
        int roulSize = this.list(Sqls.create("SELECT d.permission,c.* FROM `sys_role_menu` a  " +
                " left join sys_user_role b on a.roleId = b.roleId " +
                " left join sys_user c on b.userId = c.id " +
                " left join sys_menu d on d.id = a.menuId " +
                " where c.loginname='"+loginname+"' and d.permission='bus.ypxx.transfer.transferred'")).size();
        if(roulSize==0){
            return roulSize;
        }
        int size = this.count(Sqls.create("select count(1) from y_jbxx  jbxx where 1=1 and jbxx.lczt>0 and jbxx.delflag = 0 and jbxx.opBy = (select id from sys_user where loginname ='"+loginname+"') and jbxx.lzzt = 0 "));
        return size;
    }
    @CacheResult(cacheKey = "${args[0]}_getToReceivedCount")
    public int getToReceivedCount(String loginname) {
        //待接收数量
        int roulSize = this.list(Sqls.create("SELECT d.permission,c.* FROM `sys_role_menu` a  " +
                " left join sys_user_role b on a.roleId = b.roleId " +
                " left join sys_user c on b.userId = c.id " +
                " left join sys_menu d on d.id = a.menuId " +
                " where c.loginname='"+loginname+"' and d.permission='bus.ypxx.transfer.toReceived'")).size();
        if(roulSize==0){
            return roulSize;
        }
        int size = this.count(Sqls.create("select count(1) from y_jbxx  jbxx where 1=1 and jbxx.lczt>0 and jbxx.delflag = 0 and jbxx.jsr = (select id from sys_user where loginname ='"+loginname+"') and jbxx.lzzt in (1,2) and jbxx.status = 0"));
        return size;
    }

    @CacheResult(cacheKey = "${args[0]}_getAdReceivedCount")
    public int getAdReceivedCount(String loginname) {
        //待已收数量
        int roulSize = this.list(Sqls.create("SELECT d.permission,c.* FROM `sys_role_menu` a  " +
                " left join sys_user_role b on a.roleId = b.roleId " +
                " left join sys_user c on b.userId = c.id " +
                " left join sys_menu d on d.id = a.menuId " +
                " where c.loginname='"+loginname+"' and d.permission='bus.ypxx.transfer.adReceived'")).size();
        if(roulSize==0){
            return roulSize;
        }
        int size = this.count(Sqls.create("select count(1) from y_jbxx  jbxx where 1=1 and jbxx.lczt>0 and jbxx.delflag = 0 and jbxx.jsr = (select id from sys_user where loginname ='"+loginname+"') and jbxx.lzzt in (1,2) and (jbxx.status = 1 or jbxx.status = '-1')"));
        return size;
    }
    @CacheRemove(cacheKey = "${args[0]}_*")
    //可以通过el表达式加 * 通配符来批量删除一批缓存
    public void deleteCache(String loginname) {

    }

    @CacheRemoveAll
    public void clearCache() {

    }
}
