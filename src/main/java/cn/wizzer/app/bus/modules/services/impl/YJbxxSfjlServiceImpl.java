package cn.wizzer.app.bus.modules.services.impl;

import cn.wizzer.app.bus.modules.models.YJbxxSfjl;
import cn.wizzer.app.bus.modules.services.YJbxxSfjlService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheDefaults;


/**
 * Created by wizzer on 2016/12/22.
 */
@IocBean(args = {"refer:dao"})
@CacheDefaults(cacheName = "Y_Jbxx_sfjl")
public class YJbxxSfjlServiceImpl extends BaseServiceImpl<YJbxxSfjl> implements YJbxxSfjlService {
    public YJbxxSfjlServiceImpl(Dao dao) {
        super(dao);
    }


}
