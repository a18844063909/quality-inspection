package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentProcess;
import cn.wizzer.framework.base.service.BaseService;
import org.nutz.lang.util.NutMap;

import java.util.List;
import java.util.Map;

/**
 * @author tcjinxiu.com
 * @time 2024-04-11 10:19:44
 *
 */
public interface BusEquipmentProcessService extends BaseService<BusEquipmentProcess> {
    public List<NutMap> listThisProcess(Map all , int[] process);
    public int getNextProcess(int[] processType, int process, int status);
    public String getQueryProcess(int[] processType, String process);
    public String getOperationProcess(int[] processType, String process);
}
