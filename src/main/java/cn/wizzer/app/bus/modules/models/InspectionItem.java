package cn.wizzer.app.bus.modules.models;

import cn.wizzer.app.sys.modules.models.Sys_dict;
import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;

/**
 * 检验项
 */
@Data
@Table("bus_ins_item")
public class InspectionItem extends BaseModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column
    @Name
    @Comment("ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    @Prev(els = {@EL("uuid()")})
    private String id;


    @Column
    @Comment("检验标准")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String insStandardId;

    @Column
    @Comment("检验项目名称")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String inspectionItemName;
    @Column
    @Comment("附页序号")
    @ColDefine(type = ColType.INT, width = 3)
    private Integer inspectionItemOrder;

    @Column
    @Comment("检验项目对应标准条款号")
    @ColDefine(type = ColType.VARCHAR, width = 20)
    private String standardTermsNumber;

    @Column
    @Comment("规格型号")
    @ColDefine(type = ColType.VARCHAR, width = 20)
    private String specification;

    @Column
    @Comment("样品数量")
    @ColDefine(type = ColType.INT, width = 10)
    private Integer sampleCount;

    @Column
    @Comment("检验费用")
    @ColDefine(type = ColType.FLOAT, width = 10)
    private Float inspectionPrice;

    @Column
    @Comment("收费标准编码")
    @ColDefine(type = ColType.VARCHAR, width = 20)
    private String chargeStandardCode;

    @Column
    @Comment("收费标准名称")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String chargeStandardName;

    @Column
    @Comment("计量单位")
    @ColDefine(type = ColType.VARCHAR, width = 20)
    private String countUnit;

    @Column
    @Comment("检验周期")
    @ColDefine(type = ColType.VARCHAR, width = 20)
    private String inspectionCycle;

    @Column
    @Comment("环境要求")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String environmentStandard;

    @Column
    @Comment("设备编号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String equipmentId;
    @Column
    @Comment("设备编号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String equipmentCode;

    @Column
    @Comment("设备名称")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String equipmentName;

    @Column
    @Comment("备注")
    @ColDefine(type = ColType.VARCHAR, width = 225)
    private String remark;

    /**
     * 检验标准
     */
    @One(field = "insStandardId")
    private InspectionStandard inspectionStandard;

    /**
     * 收费标准
     */
    @One(field = "chargeStandardCode", key = "code")
    private Sys_dict chargeStandard;

    /**
     * 设备
     */
    @One(field = "equipmentCode", key = "code")
    private Sys_dict equipment;

}
