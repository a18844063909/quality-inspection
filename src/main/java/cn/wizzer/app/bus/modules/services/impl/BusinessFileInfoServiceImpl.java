package cn.wizzer.app.bus.modules.services.impl;

import cn.wizzer.app.bus.modules.models.BusinessFileInfo;
import cn.wizzer.app.bus.modules.services.BusinessFileInfoService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheRemove;
import org.nutz.plugins.wkcache.annotation.CacheRemoveAll;

@IocBean(args = {"refer:dao"})
public class BusinessFileInfoServiceImpl extends BaseServiceImpl<BusinessFileInfo> implements BusinessFileInfoService {
    public BusinessFileInfoServiceImpl(Dao dao) {
        super(dao);
    }

    @CacheRemoveAll
    public void clearCache() {}

    @CacheRemove(cacheKey = "${args[0]}_*")
    //可以通过el表达式加 * 通配符来批量删除一批缓存
    public void deleteCache(String reportTemplateId) {}
}
