package cn.wizzer.app.bus.modules.services.impl;



import cn.wizzer.app.bus.modules.models.equipment.BusEquipment;
import cn.wizzer.app.bus.modules.services.BusEquipmentService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;

@IocBean(args = {"refer:dao"})
public class BusEquipmentServiceImpl extends BaseServiceImpl<BusEquipment> implements BusEquipmentService {
    public BusEquipmentServiceImpl(Dao dao) {
        super(dao);
    }

}
