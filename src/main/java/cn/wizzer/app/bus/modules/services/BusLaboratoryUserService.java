package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratoryEquipment;
import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratoryUser;
import cn.wizzer.framework.base.service.BaseService;

/**
 * @author tcjinxiu.com
 * @time 2024-04-11 10:19:44
 *
 */
public interface BusLaboratoryUserService extends BaseService<BusLaboratoryUser> {
}
