package cn.wizzer.app.bus.modules.services.impl;

import cn.wizzer.app.bus.modules.models.BusCompany;
import cn.wizzer.app.bus.modules.models.BusTerm;
import cn.wizzer.app.bus.modules.services.BusCompanyService;
import cn.wizzer.app.bus.modules.services.BusTermService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;

@IocBean(args = {"refer:dao"})
public class BusTermServiceImpl extends BaseServiceImpl<BusTerm> implements BusTermService {
    public BusTermServiceImpl(Dao dao) {
        super(dao);
    }

}
