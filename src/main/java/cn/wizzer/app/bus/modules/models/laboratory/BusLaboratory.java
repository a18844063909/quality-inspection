package cn.wizzer.app.bus.modules.models.laboratory;

import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentInspectionInventory;
import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
* @author tcjinxiu.com
* @time   2024-04-11 10:19:44
*/
@Data
@Table("bus_laboratory")
public class BusLaboratory extends BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column
	@Name
	@Prev(els = {@EL("uuid()")})
	private String id;
	@Column
	private String name;
	@Column
	private String address;
	@Column
	private String major;
	@Column
	private String managerName;
	@Column
	private String managerBy;
	@Column
	private String depts;
	@Column
	private String deptsName;
	@Column
	private String remark;
	@Column
	private String fileId;

	@Many(field = "laboratoryId")
	private List<BusLaboratoryEquipment> equipments;

	@Many(field = "laboratoryId")
	private List<BusLaboratoryUser> users;


}
