package cn.wizzer.app.bus.modules.models.equipment;
import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.integration.json4excel.annotation.J4EIgnore;
import org.nutz.integration.json4excel.annotation.J4EName;

/**
* @author tcjinxiu.com
* @time   2024-04-11 10:19:44
*/
@Data
@J4EName("设备使用记录")
@Table("bus_equipment_record")
public class BusEquipmentRecord extends BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column
	@Name
	@Comment("ID")
	@J4EIgnore
	@Prev(els = {@EL("uuid()")})
	private String id;
	@Column
	@J4EIgnore
	private String equipmentId;
	@Column
	@J4EIgnore
	private String jbxxId;
	@Column
	@J4EName("设备编号")
	@Readonly
	private String code;
	@Column
	@J4EName("申请状态")
	private String lczt;
	@Column
	@J4EName("设备名称")
	@Readonly
	private String name;
	@Column
	@J4EName("规格型号")
	@Readonly
	private String model;
	@Column
	@J4EName("设备责任人")
	@Readonly
	private String managerName;
	@Column
	@J4EName("使用部门")
	@Readonly
	private String applyName;
	@Column
	@J4EName("样品编号")
	private String ypbh;

	@Column
	@J4EName("使用人")
	@Readonly
	private String opName;
	@Column
	@J4EName("使用开始时间")
	private String startTime;
	@Column
	@J4EName("使用结束时间")
	private String endTime;
	@Column
	@J4EName("使用前状态")
	private String startStatus;
	@Column
	@J4EName("使用后状态")
	private String endStatus;
	@Column
	@J4EName("温度℃")
	private String heat;
	@Column
	@J4EName("湿度%")
	private String humidity;
	/*@Column
	private String recordBy;*/
	@Column
	@J4EIgnore
	private String fileId;
	@Column
	@J4EIgnore
	private String auditFlag;
	@Column
	@J4EIgnore
	private String nextsprIds;
	@Column
	@J4EIgnore
	private String nextspr;


	@Column
	@J4EName("创建时间")
	@Readonly
	private String opTime;
}
