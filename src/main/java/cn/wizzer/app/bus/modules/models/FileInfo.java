package cn.wizzer.app.bus.modules.models;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class FileInfo  implements Serializable {
    @JsonProperty("BaseFileName")
    private String baseFileName;
    @JsonProperty("OwnerId")
    private String ownerId;
    @JsonProperty("Size")
    private long size;
    @JsonProperty("SHA256")
    private String sha256;
    @JsonProperty("Version")
    private long version;

    // 是否允许连接到文件中引用的外部服务（例如，可嵌入javascript应用程序）
    @JsonProperty("AllowExternalMarketplace")
    private boolean allowExternalMarketplace = true;
    // 是否有权限修改
    @JsonProperty("UserCanWrite")
    private boolean userCanWrite = true;
    // 是否支持更新
    @JsonProperty("SupportsUpdate")
    private boolean supportsUpdate = true;
    // 是否支持锁定
    @JsonProperty("SupportsLocks")
    private boolean supportsLocks = true;
    /**
     * 其他字段
     */
    // 是否支持使用WOPI客户端创建新文件
    private boolean SupportsFileCreation = true;
    // 是否支持用户可以通过受限制的URL以有限的方式对文件进行操作的方案
    private boolean SupportsScenarioLinks = true;


    public String toString() { return "FileInfo{baseFileName='" + this.baseFileName + '\'' + ", ownerId='" + this.ownerId + '\'' + ", " +
            "size=" + this.size + ", sha256='" + this.sha256 + '\'' + ", version=" + this.version + ", " +
            "allowExternalMarketplace=" + this.allowExternalMarketplace + ", userCanWrite=" + this.userCanWrite + ", " +
            "supportsUpdate=" + this.supportsUpdate + ", supportsLocks=" + this.supportsLocks
            + ", supportsFileCreation=" + this.SupportsFileCreation + ", supportsScenarioLinks=" + this.SupportsScenarioLinks + '}'; }

}
