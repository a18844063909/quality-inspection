package cn.wizzer.app.bus.modules.models;

import cn.wizzer.framework.base.model.BaseModel;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;

/**
 * Created by yxb on 2019/11
 */
@Table("y_jbxx_lcxx")
public class YJbxxLcxx extends BaseModel implements Serializable {
    private static final long serialVersionUID = 4L;
    @Column
    @Name
    @Comment("ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    @Prev(els = {@EL("uuid()")})
    private String id;

    @Column
    @Comment("主表id")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jbxxId;

    @Column
    @Comment("样品编号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String ypbh;

    @Column
    @Comment("流程状态")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private String lczt;

    @Column
    @Comment("审批意见")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String spyj;

    @Column
    @Comment("下一步审批人")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String nextsprIds;

    @Column
    @Comment("下一步审批人")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String nextspr;

    @Column
    @Comment("检验人员")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String jyryIds;

    @Column
    @Comment("检验人员")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String jyry;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJbxxId() {
        return jbxxId;
    }

    public void setJbxxId(String jbxxId) {
        this.jbxxId = jbxxId;
    }

    public String getYpbh() {
        return ypbh;
    }

    public void setYpbh(String ypbh) {
        this.ypbh = ypbh;
    }

    public String getLczt() {
        return lczt;
    }

    public void setLczt(String lczt) {
        this.lczt = lczt;
    }

    public String getSpyj() {
        return spyj;
    }

    public void setSpyj(String spyj) {
        this.spyj = spyj;
    }

    public String getNextspr() {
        return nextspr;
    }

    public void setNextspr(String nextspr) {
        this.nextspr = nextspr;
    }

    public String getJyry() {
        return jyry;
    }

    public void setJyry(String jyry) {
        this.jyry = jyry;
    }

    public String getNextsprIds() {
        return nextsprIds;
    }

    public void setNextsprIds(String nextsprIds) {
        this.nextsprIds = nextsprIds;
    }

    public String getJyryIds() {
        return jyryIds;
    }

    public void setJyryIds(String jyryIds) {
        this.jyryIds = jyryIds;
    }
}
