package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentServicing;
import cn.wizzer.app.sys.modules.models.Sys_user;
import cn.wizzer.framework.base.service.BaseService;

/**
 * @author tcjinxiu.com
 * @time 2024-04-11 10:19:44
 *
 */
public interface BusEquipmentServicingService extends BaseService<BusEquipmentServicing> {

    int getAuditCount(Sys_user user);
    int getCompleteCount(Sys_user user);
    void deleteCache(Sys_user user);
    void clearCache();
}
