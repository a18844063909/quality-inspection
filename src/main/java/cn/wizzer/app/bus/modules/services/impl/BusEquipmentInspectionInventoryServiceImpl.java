package cn.wizzer.app.bus.modules.services.impl;


import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentInspectionInventory;
import cn.wizzer.app.bus.modules.services.BusEquipmentInspectionInventoryService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheRemove;
import org.nutz.plugins.wkcache.annotation.CacheRemoveAll;
import org.nutz.plugins.wkcache.annotation.CacheResult;

import java.util.Map;

@IocBean(args = {"refer:dao"})
public class BusEquipmentInspectionInventoryServiceImpl extends BaseServiceImpl<BusEquipmentInspectionInventory> implements BusEquipmentInspectionInventoryService {
    public BusEquipmentInspectionInventoryServiceImpl(Dao dao) {
        super(dao);
    }
}
