package cn.wizzer.app.bus.modules.models.equipment;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;

/**
* @author tcjinxiu.com
* @time   2024-04-11 10:19:44
*/
@Data
@Table("bus_equipment_report_item")
public class BusEquipmentReportItem extends BaseModel implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column
	@Name
	@Prev(els = {@EL("uuid()")})
	private String id;
	@Column
	private String reportId;
	@Column
	private String name;
	@Column
	private String res;
	@Column
	private String indicators;
	@Column
	private int sort;
}
