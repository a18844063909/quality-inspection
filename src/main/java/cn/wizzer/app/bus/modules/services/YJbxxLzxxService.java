package cn.wizzer.app.bus.modules.services;

import cn.wizzer.app.bus.modules.models.YJbxxLcxx;
import cn.wizzer.app.bus.modules.models.YJbxxLzxx;
import cn.wizzer.app.sys.modules.models.Sys_msg_user;
import cn.wizzer.framework.base.service.BaseService;

import java.util.List;

/**
 * Created by yx on 2019
 */
public interface YJbxxLzxxService extends BaseService<YJbxxLzxx> {
    int getTransferredCount(String loginname);
    int getToReceivedCount(String loginname);
    int getAdReceivedCount(String loginname);
    /**
     * 清空缓存
     */
    void clearCache();

}
