package cn.wizzer.app.bus.modules.models;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;

/**
 * Created by yxb on 2019/11
 */
@Data
@Table("y_jbxx_lzxx")
public class YJbxxLzxx extends BaseModel implements Serializable {
    private static final long serialVersionUID = 4L;
    @Column
    @Name
    @Comment("ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    @Prev(els = {@EL("uuid()")})
    private String id;

    @Column
    @Comment("主表id")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jbxxId;

    @Column
    @Comment("样品编号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String ypbh;

    @Column
    @Comment("流转状态")
    @ColDefine(type = ColType.CHAR, width = 2)
    private String lzzt;
    @Column
    @Comment("流转时间")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String lzAt;
    @Column
    @Comment("流转数量")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String number;
    @Column
    @Comment("完成期限")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jhwcqx;

    @Column
    @Comment("备注")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String spyj;


    @Column
    @Comment("拒收备注")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String jsbz;

    @Column
    @Comment("下一步审批人")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String nextsprIds;

    @Column
    @Comment("下一步审批人")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String nextspr;
    @Column
    @Comment("确认时间")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String statusAt;
    @Column
    @Comment("确认状态")
    @ColDefine(type = ColType.VARCHAR, width = 2)
    private String status;
    @Column
    @Comment("检验完成日期")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jywcrq;
    @Column
    @Comment("试毕样品处理情况")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String byczqk;

}
