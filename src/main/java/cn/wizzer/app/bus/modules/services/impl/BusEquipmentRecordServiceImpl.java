package cn.wizzer.app.bus.modules.services.impl;


import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentRecord;

import cn.wizzer.app.bus.modules.services.BusEquipmentRecordService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;

@IocBean(args = {"refer:dao"})
public class BusEquipmentRecordServiceImpl extends BaseServiceImpl<BusEquipmentRecord> implements BusEquipmentRecordService {
    public BusEquipmentRecordServiceImpl(Dao dao) {
        super(dao);
    }

}
