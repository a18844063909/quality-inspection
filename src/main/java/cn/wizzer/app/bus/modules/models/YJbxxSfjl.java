package cn.wizzer.app.bus.modules.models;

import cn.wizzer.framework.base.model.BaseModel;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;

/**
 * Created by yxb on 2019/11
 */
@Table("Y_Jbxx_sfjl")
public class YJbxxSfjl extends BaseModel implements Serializable {
    private static final long serialVersionUID = 111L;
    @Column
    @Name
    @Comment("ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    @Prev(els = {@EL("uuid()")})
    private String id;

    @Column
    @Comment("主表id")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jbxxId;

    @Column
    @Comment("样品编号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String ypbh;

    @Column
    @Comment("缴费日期")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jfrq;

    @Column
    @Comment("本次实收")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String bcss;


    @Column
    @Comment("收费状态")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String sfzt;


    @Column
    @Comment("收入分类")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String srfl;

    @Column
    @Comment("票据分类")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String pjfl;

    @Column
    @Comment("票据号码")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String pjhm;


    @Column
    @Comment("缴费方式")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String jffs;

    @Column
    @Comment("凭证号")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String pzh;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getYpbh() {
        return ypbh;
    }

    public void setYpbh(String ypbh) {
        this.ypbh = ypbh;
    }

    public String getBcss() {
        return bcss;
    }

    public void setBcss(String bcss) {
        this.bcss = bcss;
    }


    public String getSfzt() {
        return sfzt;
    }

    public void setSfzt(String sfzt) {
        this.sfzt = sfzt;
    }


    public String getSrfl() {
        return srfl;
    }

    public void setSrfl(String srfl) {
        this.srfl = srfl;
    }

    public String getPjfl() {
        return pjfl;
    }

    public void setPjfl(String pjfl) {
        this.pjfl = pjfl;
    }

    public String getPjhm() {
        return pjhm;
    }

    public void setPjhm(String pjhm) {
        this.pjhm = pjhm;
    }

    public String getJffs() {
        return jffs;
    }

    public void setJffs(String jffs) {
        this.jffs = jffs;
    }

    public String getPzh() {
        return pzh;
    }

    public void setPzh(String pzh) {
        this.pzh = pzh;
    }

    public String getJbxxId() {
        return jbxxId;
    }

    public void setJbxxId(String jbxxId) {
        this.jbxxId = jbxxId;
    }

    public String getJfrq() {
        return jfrq;
    }

    public void setJfrq(String jfrq) {
        this.jfrq = jfrq;
    }
}
