package cn.wizzer.app.bus.modules.services.impl;



import cn.wizzer.app.bus.modules.models.equipment.BusEquipment;
import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratory;
import cn.wizzer.app.bus.modules.services.BusEquipmentService;
import cn.wizzer.app.bus.modules.services.BusLaboratoryService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;

@IocBean(args = {"refer:dao"})
public class BusLaboratoryServiceImpl extends BaseServiceImpl<BusLaboratory> implements BusLaboratoryService {
    public BusLaboratoryServiceImpl(Dao dao) {
        super(dao);
    }

}
