package cn.wizzer.app.bus.modules.services.impl;



import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentReport;
import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentReportItem;
import cn.wizzer.app.bus.modules.services.BusEquipmentReportItemService;
import cn.wizzer.app.bus.modules.services.BusEquipmentReportService;
import cn.wizzer.app.sys.modules.models.Sys_user;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheRemove;
import org.nutz.plugins.wkcache.annotation.CacheRemoveAll;
import org.nutz.plugins.wkcache.annotation.CacheResult;

@IocBean(args = {"refer:dao"})
public class BusEquipmentReportItemServiceImpl extends BaseServiceImpl<BusEquipmentReportItem> implements BusEquipmentReportItemService {
    public BusEquipmentReportItemServiceImpl(Dao dao) {
        super(dao);
    }

}
