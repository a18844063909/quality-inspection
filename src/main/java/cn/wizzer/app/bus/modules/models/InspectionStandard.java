package cn.wizzer.app.bus.modules.models;

import cn.wizzer.app.sys.modules.models.Sys_dict;
import cn.wizzer.app.sys.modules.models.Sys_unit;
import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 检验标准类
 */
@Data
@Table("bus_ins_standard")
public class InspectionStandard extends BaseModel  implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column
    @Name
    @Comment("ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    @Prev(els = {@EL("uuid()")})
    private String id;

    @Column
    @Comment("产品名称")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String productName;

    @Column
    @Comment("产品类型编码")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String productTypeCode;

    @Column
    @Comment("产品类型名称")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String productTypeName;

    @Column
    @Comment("检验标准")
    @ColDefine(type = ColType.VARCHAR, width = 255)
    private String inspectionStandard;

/*    @Column
    @Comment("所属单位ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    private String unitId;

    @Column
    @Comment("所属单位名称")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String unitName;*/

    @Column
    @Comment("操作人姓名")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String operatorName;

    /**
     * 检验项
     */
    @Many(field = "insStandardId")
    private List<InspectionItem> inspectionItems;

    /**
     * 产品类型
     */
    @One(field = "productTypeCode",key = "code")
    private Sys_dict productType;


    @Column
    @Comment("是否禁用")
    @ColDefine(type = ColType.BOOLEAN)
    private boolean disabled;


    /**
     * 所属单位
     */
/*    @One(field = "unitId",key = "id")
    private Sys_unit unit;*/

}
