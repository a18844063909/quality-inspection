package cn.wizzer.app.web.modules.controllers.platform.bus;

import cn.wizzer.app.bus.modules.models.BusinessFileInfo;
import cn.wizzer.app.bus.modules.services.BusinessFileInfoService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;

@IocBean
@At("/platform/bus/file_info/")
public class BusinessFileInfoController {

    private static final Log log = Logs.get();

    @Inject
    private BusinessFileInfoService businessFileInfoService;


    @At
    @Ok("json")
    @RequiresPermissions("bus.config_management.report_template.add")
    @SLog(tag = "添加模板文件", msg = "模板名称:${businessFileInfo.infoName}")
    public Object addDo(@Param("..") BusinessFileInfo businessFileInfo, HttpServletRequest req) {
        try {
            int num = businessFileInfoService.count(
                    Cnd.where("infoName", "=", Strings.trim(businessFileInfo.getInfoName()))
                            .and("infoVersion", "=", Strings.trim(businessFileInfo.getInfoVersion()))
            );
            if (num > 0) {
                return Result.error("已存在此版本的模板文件");
            }
            businessFileInfo.setInfoName(Strings.trim(businessFileInfo.getInfoName()));
            businessFileInfo.setInfoVersion(Strings.trim(businessFileInfo.getInfoVersion()));
//            businessFileInfo.setInfoType("template");
            businessFileInfo.setRemark(Strings.trim(businessFileInfo.getRemark()));
            businessFileInfo.setOpBy(StringUtil.getPlatformUid());
            businessFileInfo.setOperatorName(StringUtil.getPlatformUsername());
            businessFileInfo.setDisabled(false);
            businessFileInfoService.insert(businessFileInfo);
            return Result.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }

}
