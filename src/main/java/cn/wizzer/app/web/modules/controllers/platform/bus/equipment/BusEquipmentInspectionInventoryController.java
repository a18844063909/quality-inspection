package cn.wizzer.app.web.modules.controllers.platform.bus.equipment;

import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentInspectionInventory;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.framework.base.Result;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.*;

import javax.servlet.http.HttpServletRequest;
import cn.wizzer.app.bus.modules.services.BusEquipmentInspectionInventoryService;

/**
 * @author tcjinxiu.com
 * @time 2024-04-11 10:19:44
 *
 */
@IocBean
@At("/platform/bus/equipment/inspection/inventory")
public class BusEquipmentInspectionInventoryController {
private static final Log log = Logs.get();
	@Inject
	BusEquipmentInspectionInventoryService busEquipmentInspectionInventoryService;

	@At({"/delete","/delete/?"})
	@Ok("json")
	@SLog(tag = "Delete", msg = "Delete:bus_equipment_inspection_inventory")
	public Object delete(String id,@Param("ids") String[] ids, HttpServletRequest req) {
		try {
			if(ids!=null&&ids.length>0){
				busEquipmentInspectionInventoryService.delete(ids);
			}else{
				busEquipmentInspectionInventoryService.delete(id);
			}
			return Result.success("system.success");
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}
	@At({"/deleteIn","/deleteIn/?"})
	@Ok("json")
	@SLog(tag = "deleteIn", msg = "Delete:bus_equipment_inspection_inventory")
	public Object deleteIn(String id,@Param("ids") String[] ids, HttpServletRequest req) {
		try {
			if(ids!=null&&ids.length>0){
				for(String i : ids){
					BusEquipmentInspectionInventory eii = busEquipmentInspectionInventoryService.fetch(i);
					eii.setInspectionInId(null);
					eii.setExterior(null);
					eii.setInStatus(null);
					busEquipmentInspectionInventoryService.update(eii);
				}
			}else{
				BusEquipmentInspectionInventory eii = busEquipmentInspectionInventoryService.fetch(id);
				eii.setInspectionInId(null);
				eii.setExterior(null);
				eii.setInStatus(null);
				busEquipmentInspectionInventoryService.update(eii);
			}
			return Result.success("system.success");
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}

}
