package cn.wizzer.app.web.modules.controllers.platform.bus;

import cn.wizzer.app.bus.modules.models.BusCompany;
import cn.wizzer.app.bus.modules.models.BusinessFileInfo;
import cn.wizzer.app.bus.modules.services.BusCompanyService;
import cn.wizzer.app.bus.modules.services.BusinessFileInfoService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.PageUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.dao.sql.Sql;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@IocBean
@At("/platform/bus/company/")
public class BusCompanyController {

    private static final Log log = Logs.get();

    @Inject
    private BusCompanyService busCompanyService;
    @At("")
    @Ok("beetl:/platform/bus/company/index.html")
    @RequiresPermissions("bus.company.index")
    public void index(HttpServletRequest req) {


    }
    @At
    @Ok("json:full")
    @RequiresPermissions("bus.company.data")
    public Object data( @Param("pageNumber") int pageNumber,@Param("pageSize") int pageSize,@Param("pageOrderName") String pageOrderName,@Param("pageOrderBy") String pageOrderBy,
            @Param("name") String name) {
        try {
            String sql = "select * from bus_company where delFlag = 0 ";
            if(StringUtils.isNotBlank(name)){
                sql+=" and name like '%"+name+"%' ";
            }
            sql+=StringUtil.dataScopeFilter("");
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                if ("ascending".equalsIgnoreCase(pageOrderBy)) {
                    sql+= " order by "+pageOrderName+" asc,opAt desc ";
                } else {
                    sql+= " order by "+pageOrderName+" desc,opAt desc ";
                }
            }else{
                sql+= " order by opAt desc ";
            }
            return Result.success().addData(busCompanyService.listPageMap(pageNumber, pageSize, Sqls.create(sql)));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At
    @Ok("json:full")
    @RequiresAuthentication
    public Object list(@Param("name") String name) {
        try {
            String sql ="select * from bus_company where delFlag=0 ";
            if(StringUtils.isNotBlank(name)){
                sql+=" and name like '%"+name+"%'";
            }
            sql+= " order by opAt desc ";
            return Result.success().addData(busCompanyService.listEntity(Sqls.create(sql)));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At
    @Ok("json")
    @RequiresPermissions("bus.company.add")
    @SLog(tag = "添加厂家信息", msg = "厂家信息:${busCompany.name}")
    public Object addDo(@Param("..") BusCompany busCompany, HttpServletRequest req) {
        try {
            busCompanyService.insert(busCompany);
            return Result.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }

    @At
    @Ok("json")
    @RequiresPermissions("bus.company.edit")
    @SLog(tag = "修改厂家信息", msg = "厂家信息:${busCompany.name}")
    public Object editDo(@Param("..") BusCompany busCompany, HttpServletRequest req) {
        try {
            busCompanyService.update(busCompany);
            return Result.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At("/detail/?")
    @Ok("json")
    @RequiresPermissions("bus.company.detail")
    public Object detail(String id, HttpServletRequest req) {
        try {
            BusCompany busCompany = busCompanyService.fetch(id);
            return Result.success(busCompany);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At("/delete/?")
    @Ok("json")
    @RequiresPermissions("bus.company.delete")
    @SLog(tag = "删除厂家信息", msg = "厂家ID:${args[1].getAttribute('id')}")
    public Object delete(String id, HttpServletRequest req) {
        try {
            //逻辑删除
            busCompanyService.vDelete(id);
            return Result.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
}
