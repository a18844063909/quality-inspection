package cn.wizzer.app.web.modules.controllers.platform.bus;

import cn.wizzer.app.bus.modules.models.BusCompany;
import cn.wizzer.app.bus.modules.models.BusTerm;
import cn.wizzer.app.bus.modules.services.BusTermService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.PageUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@IocBean
@At("/platform/bus/term/")
public class BusTermController {

    private static final Log log = Logs.get();

    @Inject
    private BusTermService busTermService;
    @At("")
    @Ok("beetl:/platform/bus/configManagement/term/index.html")
    @RequiresPermissions("bus.term.index")
    public void index(HttpServletRequest req) {


    }
    @At
    @Ok("json:full")
    @RequiresPermissions("bus.term.data")
    public Object data( @Param("pageNumber") int pageNumber,@Param("pageSize") int pageSize,@Param("pageOrderName") String pageOrderName,@Param("pageOrderBy") String pageOrderBy,
            @Param("content") String content) {
        try {
            Cnd cnd = Cnd.NEW();
            Optional.ofNullable(content)
                    .filter(searchKey -> searchKey.trim().length() > 0)
                    .map(searchKey -> cnd.and("content", "like", "%" + searchKey + "%"));
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                cnd.orderBy(pageOrderName, PageUtil.getOrder(pageOrderBy));
            }else{
                cnd.desc("opAt");
            }
            cnd.and("delFlag", "=", 0);
            cnd.and("opBy","=",StringUtil.getPlatformUid());
            return Result.success().addData(busTermService.listPage(pageNumber, pageSize, cnd));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At
    @Ok("json:full")
    @RequiresAuthentication
    public Object list(@Param("content") String content) {
        try {
            String sql ="select * from bus_term where delFlag=0 and opBy = '"+StringUtil.getPlatformUid()+"' ";
            //sql+=StringUtil.dataScopeFilter("");

            if(StringUtils.isNotBlank(content)){
                sql+=" and content like '%"+content+"%'";
            }
            return Result.success().addData(busTermService.listEntity(Sqls.create(sql)));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At
    @Ok("json")
    @RequiresPermissions("bus.term.add")
    public Object addDo(@Param("..") BusTerm busTerm, HttpServletRequest req) {
        try {
            busTermService.insert(busTerm);
            return Result.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }

    @At
    @Ok("json")
    @RequiresPermissions("bus.term.edit")
    @SLog(tag = "修改用语", msg = "厂家信息:${busTerm.content}")
    public Object editDo(@Param("..") BusTerm busTerm, HttpServletRequest req) {
        try {
            busTermService.update(busTerm);
            return Result.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At("/detail/?")
    @Ok("json")
    @RequiresAuthentication
    public Object detail(String id, HttpServletRequest req) {
        try {
            BusTerm busTerm = busTermService.fetch(id);
            return Result.success(busTerm);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At("/delete/?")
    @Ok("json")
    @RequiresPermissions("bus.term.delete")
    @SLog(tag = "删除用语", msg = "termID:${args[1].getAttribute('id')}")
    public Object delete(String id, HttpServletRequest req) {
        try {
            //逻辑删除
            busTermService.vDelete(id);
            return Result.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
}
