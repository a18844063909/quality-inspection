package cn.wizzer.app.web.modules.controllers.platform.bus.equipment;

import cn.wizzer.app.bus.modules.models.equipment.BusEquipment;
import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentCheck;
import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentProcess;
import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentReport;
import cn.wizzer.app.bus.modules.services.BusEquipmentCheckService;
import cn.wizzer.app.bus.modules.services.BusEquipmentProcessService;
import cn.wizzer.app.bus.modules.services.BusEquipmentService;
import cn.wizzer.app.sys.modules.services.SysDictService;
import cn.wizzer.app.sys.modules.services.SysUnitService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.DateUtil;
import cn.wizzer.app.web.commons.utils.ShiroUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.aop.interceptor.ioc.TransAop;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.ioc.aop.Aop;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.lang.Times;
import org.nutz.lang.util.NutMap;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.*;
import org.nutz.trans.Atom;
import org.nutz.trans.Trans;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author tcjinxiu.com
 * @time 2024-04-11 10:19:44
 *
 */
@IocBean
@At("/platform/bus/equipment/check")
public class BusEquipmentCheckController {
	private static final Log log = Logs.get();
	@Inject
	BusEquipmentCheckService busEquipmentCheckService;
	@Inject
	BusEquipmentService busEquipmentService;
	@Inject
	BusEquipmentProcessService busEquipmentProcessService;
	@Inject
	private SysDictService sysDictService;
	@Inject
	private SysUnitService sysUnitService;
	@Inject
	private SysUserService sysUserService;
	@Inject
	private ShiroUtil shiroUtil;

	private static final String type="check";
	//点检流程"ask audit complete


	@At("/?")
	@Ok("beetl:/platform/bus/equipment/check/index.html")
	@RequiresAuthentication
	public void index(String processName,HttpServletRequest req) {
		req.setAttribute("userId",StringUtil.getPlatformUid());
		req.setAttribute("processName", processName);
		req.setAttribute("processMap", JSONObject.fromObject(sysDictService.getCodeMap("equipmentProcessCode")));
		List<NutMap> list = sysDictService.dictCodeTree("equipmentProcessCode");
		list.remove(list.size()-1);
		req.setAttribute("processOptions", JSONArray.fromObject(list));
		req.setAttribute("calculateMap", JSONObject.fromObject(sysDictService.getCodeMap("calculate")));
		req.setAttribute("calculateCycleTypeMap", JSONObject.fromObject(sysDictService.getCodeMap("calculateCycleType")));

	}
	@At
	@Ok("json:full")
	@RequiresAuthentication
	public Object data(@Param("code") String code, @Param("name") String name,@Param("lczt") String lczt,
					   @Param("processName") String processName,
					   @Param("pageNumber") int pageNumber,@Param("pageSize") int pageSize
			, @Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy) {
		try {
			String sql = "select e.code,e.name,e.model,e.company,e.purchaseDate,e.purchasePrice," +
					" ur.username as managerName,ut.name as applyName,urr.username as opName, " +
					" es.id,es.lczt,es.nextspr,es.checkTime,es.checkStatus,es.cause,es.maintenance,es.servicing,es.servicingId,FROM_UNIXTIME(es.opAt) as opTime,es.opBy " +
					" from bus_equipment_check es  " +
					" left join bus_equipment e on es.equipmentId=e.id " +
					" left join sys_user ur on ur.id=e.managerBy " +
					" left join sys_unit ut on ut.id = e.applyDept " +
					" left join sys_user urr on urr.id=es.opBy " +
					" where es.delFlag=0 ";
			if(StringUtils.isNotBlank(processName)&& processName.equals("audit")){
				sql+= " and es.nextsprIds='"+StringUtil.getPlatformUid()+"' and (es.lczt = 2 or es.lczt = 1)";
			}else{
				//数据权限
				sql+= StringUtil.dataScopeFilter("es");
			}
			if(StringUtils.isNotBlank(code)){
				sql+= " and  e.code like '%"+code+"%' ";
			}
			if(StringUtils.isNotBlank(name)){
				sql+= " and  e.name like '%"+name+"%' ";
			}
			if(StringUtils.isNotBlank(lczt)){
				sql+= " and  es.lczt = '"+lczt+"' ";
			}

			sql="select * from ("+sql+") a";
			if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
				if ("ascending".equalsIgnoreCase(pageOrderBy)) {
					sql+= " order by "+pageOrderName+" asc,opTime desc ";
				} else {
					sql+= " order by "+pageOrderName+" desc,opTime desc ";
				}
			}else{
				sql+= " order by opTime desc ";
			}
			return Result.success().addData(busEquipmentCheckService.listPage(pageNumber, pageSize, Sqls.create(sql)));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Result.error();
		}
	}
	@Aop(TransAop.READ_COMMITTED)
	@At
	@Ok("json")
	@RequiresPermissions("equipment.check.add")
	@SLog(tag = "Add", msg = "Add:bus_equipment_check")
	public Object addDo(@Param("..") BusEquipmentCheck check, @Param("..") BusEquipmentProcess process,HttpServletRequest req) {
		try {
			check.setAuditFlag("0");
			check = busEquipmentCheckService.insert(check);
			process.setHeadId(check.getId());
			process.setEquipmentId(check.getEquipmentId());
			process.setLczt(check.getLczt());
			process.setType(type);
			process.setStatus("0");
			busEquipmentProcessService.insert(process);
			busEquipmentCheckService.clearCache();
			return Result.success("system.success");
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}

	@At("/detail/?")
	@Ok("json")
	public Object detail(String id) {
		BusEquipmentCheck check = busEquipmentCheckService.fetch(id);
		List listProcess = busEquipmentProcessService.list(Sqls.create("select *,(select username from sys_user where id = es.opBy) as opName,FROM_UNIXTIME(es.opAt) as opTime from  bus_equipment_process es where " +
				" equipmentId='"+check.getEquipmentId()+"' and headId='"+check.getId()+"' and type='"+type+"' order by opAt desc"));
		NutMap map = new NutMap();
		map.addv("check", check);
		map.addv("listProcess", listProcess);
		try {
			return Result.success("system.success").addData(map);
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}

	@Aop(TransAop.READ_COMMITTED)
	@At
	@Ok("json")
	@RequiresPermissions("equipment.check.edit")
	@SLog(tag = "Edit", msg = "Edit:bus_equipment_check")
	public Object editDo(@Param("..") BusEquipmentCheck check,@Param("..") BusEquipmentProcess process,HttpServletRequest req) {
		try {
			check.setAuditFlag("0");
			process.setHeadId(check.getId());
			process.setEquipmentId(check.getEquipmentId());
			process.setLczt(check.getLczt());
			process.setType(type);
			process.setStatus("0");
			busEquipmentCheckService.update(check);
			busEquipmentProcessService.execute(Sqls.create("update bus_equipment_process set delflag=1 where lczt>=" + check.getLczt() + " and headId='" + check.getId() + "' and type='" + type + "' "));
			busEquipmentProcessService.insert(process);

			busEquipmentCheckService.clearCache();
			return Result.success("system.success");
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}

	@Aop(TransAop.READ_COMMITTED)
	@At("/subDo/?")
	@Ok("json")
	@RequiresPermissions("equipment.check.edit")
	@SLog(tag = "subDo", msg = "subDo:bus_equipment_check")
	public Object subDo(String id) {
		try {
			BusEquipmentCheck check = busEquipmentCheckService.fetch(id);
			check.setLczt("1");
			BusEquipmentProcess process = busEquipmentProcessService.fetch(Cnd.NEW().and("headId", "=", check.getId())
							.and("lczt","=","0").and("delflag","=","0").and("type", "=", type));
			process.setHeadId(check.getId());
			process.setEquipmentId(check.getEquipmentId());
			process.setLczt(check.getLczt());
			process.setType(type);
			process.setStatus("0");
			process.setOpBy(StringUtil.getPlatformUid());
			process.setOpAt(Times.getTS());
			busEquipmentCheckService.update(check);
			busEquipmentProcessService.execute(Sqls.create("update bus_equipment_process set delflag=1 where lczt>=" + check.getLczt() + " and headId='" + check.getId() + "' and type='" + type + "' "));
			busEquipmentProcessService.insert(process);
			busEquipmentCheckService.clearCache();

			return Result.success("system.success");
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}

	@Aop(TransAop.READ_COMMITTED)
	@At
	@Ok("json")
	@SLog(tag = "processDo", msg = "processDo:bus_equipment_check")
	public Object processDo(@Param("..") BusEquipmentCheck check,@Param("..") BusEquipmentProcess process,
							HttpServletRequest req) {
		try {
			if(check.getAuditFlag().equals("0")){
				check.setLczt("2");
			}else if(process.getStatus().equals("0")){
				check.setLczt("4");
				check.setNextspr(null);
				check.setNextsprIds(null);
				process.setNextspr(null);
				process.setNextsprIds(null);
			}else{
				check.setLczt("3");
				check.setNextspr(null);
				check.setNextsprIds(null);
				process.setNextspr(null);
				process.setNextsprIds(null);
			}
			process.setHeadId(check.getId());
			process.setEquipmentId(check.getEquipmentId());
			process.setLczt(check.getLczt());
			process.setType(type);
			process.setOpBy(StringUtil.getPlatformUid());
			process.setOpAt(Times.getTS());
			//先清除流程
			busEquipmentCheckService.update(check);
			busEquipmentProcessService.insert(process);
			busEquipmentCheckService.clearCache();
			return Result.success("system.success");
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}


	@At({"/delete","/delete/?"})
	@Ok("json")
	@RequiresPermissions("equipment.check.delete")
	@SLog(tag = "Delete", msg = "Delete:bus_equipment_check")
	public Object delete(String id,@Param("ids") String[] ids, HttpServletRequest req) {
		try {
			if(ids!=null&&ids.length>0){
				busEquipmentCheckService.vDelete(ids);
			}else{
				busEquipmentCheckService.vDelete(id);
			}
			busEquipmentCheckService.clearCache();
			return Result.success("system.success");
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}

}
