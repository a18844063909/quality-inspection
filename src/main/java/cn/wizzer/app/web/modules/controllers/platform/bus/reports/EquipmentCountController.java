package cn.wizzer.app.web.modules.controllers.platform.bus.reports;

import cn.wizzer.app.bus.modules.models.equipment.BusEquipment;
import cn.wizzer.app.bus.modules.services.BusEquipmentService;
import cn.wizzer.app.sys.modules.services.SysDictService;
import cn.wizzer.app.sys.modules.services.SysUnitService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.utils.DateUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.nutz.dao.Sqls;
import org.nutz.integration.json4excel.J4E;
import org.nutz.integration.json4excel.J4EColumn;
import org.nutz.integration.json4excel.J4EConf;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.lang.util.NutMap;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author tcjinxiu.com
 * @time 2024-04-11 10:19:44
 *
 */
@IocBean
@At("/platform/bus/reports/equipmentCount")
public class EquipmentCountController {
private static final Log log = Logs.get();
	@Inject
	BusEquipmentService busEquipmentService;
	@Inject
	private SysDictService sysDictService;
	@Inject
	private SysUnitService sysUnitService;
	@Inject
	private SysUserService sysUserService;

	@At("")
	@Ok("beetl:/platform/bus/reports/equipmentCount/index.html")
	@RequiresAuthentication
	public void index(HttpServletRequest req) {
		req.setAttribute("nowYear",DateUtil.getDate().substring(0,4));
	}



	@At
	@Ok("json:full")
	@RequiresAuthentication
	/*@RequiresPermissions("equipment.index.data")*///不要权限，因为其它功能需要读取
	public Object data(@Param("code") String code, @Param("name") String name,@Param("year") String year,
					   @Param("pageNumber") int pageNumber,@Param("pageSize") int pageSize
                       ,@Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy) {
		try {
			String sqlYdear = " and substr(FROM_UNIXTIME(opAt),1,4)='"+year+"') ";
			String sql = "select * from (select e.name,e.code,ur.username as managerName,ut.name as applyName," +
					" (select count(1) from bus_equipment_status where lczt=4 and equipmentStatus = 2 and equipmentId=e.id "+sqlYdear+" as statusCount ,   " +
					" (select count(1) from bus_equipment_servicing where lczt=5 and equipmentId=e.id   "+sqlYdear+" as servicingCount ,  " +
					" (select count(1) from bus_equipment_report where lczt=4 and equipmentId=e.id   "+sqlYdear+" as reportCount ,  " +
					" (select count(1) from bus_equipment_record where lczt=1 and equipmentId=e.id   "+sqlYdear+" as recordCount ,  " +
					" (select count(1) from bus_equipment_portable where lczt=5 and equipmentId=e.id   "+sqlYdear+" as portableCount ,   " +
					" (select count(1) from bus_equipment_inspection i left join bus_equipment_inspection_inventory ii on i.id=ii.inspectionId where i.lczt=1 and i.type=0 and ii.equipmentId=e.id and substr(FROM_UNIXTIME(i.opAt),1,4)='"+year+"') as inspectionCount ,   " +
					" (select count(1) from bus_equipment_check where lczt=4 and equipmentId=e.id   "+sqlYdear+" as checkCount   " +
					" from bus_equipment e   " +
					" left join sys_user ur on ur.id=e.managerBy   " +
					" left join sys_unit ut on ut.id = e.applyDept where e.delFlag=0 and e.status !='6' ";
			if(StringUtils.isNotBlank(code)){
				sql+= " and  e.code like '%"+code+"%' ";
			}
			if(StringUtils.isNotBlank(name)){
				sql+= " and  e.name like '%"+name+"%' ";
			}
			sql+= " ) a";
			if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
				if ("ascending".equalsIgnoreCase(pageOrderBy)) {
					sql+= " order by "+pageOrderName+" asc,code asc ";
				} else {
					sql+= " order by "+pageOrderName+" desc,code asc ";
				}
			}else{
				sql+= " order by code asc ";
			}
			return Result.success().addData(busEquipmentService.listPage(pageNumber, pageSize, Sqls.create(sql)));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Result.error();
		}
	}



}
