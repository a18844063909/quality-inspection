package cn.wizzer.app.web.modules.controllers.platform.bus.bggl;

import cn.wizzer.app.bus.modules.models.YJbxx;
import cn.wizzer.app.bus.modules.models.YJbxxLcxx;
import cn.wizzer.app.bus.modules.services.YJbxxLcxxService;
import cn.wizzer.app.bus.modules.services.YJbxxService;
import cn.wizzer.app.sys.modules.services.SysDictService;
import cn.wizzer.app.sys.modules.services.SysUnitService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.doc.DocUtils;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.ShiroUtil;
import cn.wizzer.framework.base.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.trans.Atom;
import org.nutz.trans.Trans;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;


/**
 * 报告审核  虞秀斌  2019-11
 */
@IocBean
@At("/platform/bus/bggl")
public class UnlockController {
    private static final Log log = Logs.get();
    @Inject
    private YJbxxService yjbxxService;
    @Inject
    private YJbxxLcxxService yjbxxlcxxservice;
    @Inject
    private SysUnitService sysUnitService;
    @Inject
    private SysDictService sysDictService;
    @Inject
    private SysUserService sysUserService;
    @Inject
    private ShiroUtil shiroUtil;

    @At
    @Ok("json")
    @RequiresPermissions("bus.bggl")
    @SLog(tag = "报告解锁", msg = "样品:${args[0].ypbh}")
    public Object unlockDo(@Param("..") YJbxxLcxx obj) {
        try {
            YJbxx jbxx = yjbxxService.fetch(obj.getJbxxId());
            Cnd cnd = Cnd.NEW();
            cnd.and("jbxxid", "=", obj.getJbxxId());
            cnd.and("lczt", "=", "2");
            cnd.and("delflag", "=", "0");
            YJbxxLcxx lcxx = yjbxxlcxxservice.fetch(cnd);
            obj.setNextsprIds(lcxx.getNextsprIds());
            obj.setNextspr(lcxx.getNextspr());
            jbxx.setLczt(obj.getLczt());
            jbxx.setDyzt("0");
            Trans.exec(Connection.TRANSACTION_SERIALIZABLE, new Atom() {
                public void run() {
                    yjbxxlcxxservice.execute(Sqls.create("update y_jbxx_lcxx set delflag=1 where lczt>="+jbxx.getLczt()+" and jbxxid='"+jbxx.getId()+"'"));
                    yjbxxlcxxservice.insert(obj);
                    yjbxxService.update(jbxx);
                    yjbxxlcxxservice.clearCache();
                }
            });
            return Result.success("解锁成功！");
        } catch (Exception e) {
            return Result.error("system.error");
        }
    }
}
