package cn.wizzer.app.web.modules.controllers.platform.sys;

import cn.wizzer.app.bus.modules.models.BusinessFileInfo;
import cn.wizzer.app.bus.modules.services.BusinessFileInfoService;
import cn.wizzer.app.sys.modules.models.Sys_menu;
import cn.wizzer.app.sys.modules.models.Sys_role;
import cn.wizzer.app.sys.modules.models.Sys_unit;
import cn.wizzer.app.sys.modules.models.Sys_user;
import cn.wizzer.app.sys.modules.services.SysMenuService;
import cn.wizzer.app.sys.modules.services.SysUnitService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.PageUtil;
import cn.wizzer.app.web.commons.utils.ShiroUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.nutz.aop.interceptor.ioc.TransAop;
import org.nutz.dao.Chain;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.dao.util.cri.SqlExpression;
import org.nutz.dao.util.cri.SqlExpressionGroup;
import org.nutz.dao.util.cri.Static;
import org.nutz.integration.json4excel.J4E;
import org.nutz.integration.json4excel.J4EColumn;
import org.nutz.integration.json4excel.J4EConf;
import org.nutz.ioc.aop.Aop;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.nutz.lang.Times;
import org.nutz.lang.random.R;
import org.nutz.lang.util.NutMap;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.adaptor.JsonAdaptor;
import org.nutz.mvc.annotation.AdaptBy;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by wizzer on 2016/6/23.
 */
@IocBean
@At("/platform/sys/user")
public class SysUserController {
    private static final Log log = Logs.get();
    @Inject
    private SysUserService sysUserService;
    @Inject
    private SysMenuService sysMenuService;
    @Inject
    private SysUnitService sysUnitService;
    @Inject
    private ShiroUtil shiroUtil;

    @Inject
    private BusinessFileInfoService businessFileInfoService;

    @At("")
    @Ok("beetl:/platform/sys/user/index.html")
    @RequiresPermissions("sys.manager.user")
    public void index() {

    }
    @At("/list")
    @Ok("beetl:/platform/sys/user/list.html")
    @RequiresPermissions("sys.manager.list")
    public void list() {

    }

    @At
    @Ok("json")
    @RequiresPermissions("sys.manager.user.add")
    @SLog(tag = "新建用户", msg = "用户名:${args[0].loginname}")
    @Aop(TransAop.READ_COMMITTED)
    @AdaptBy(type = JsonAdaptor.class)
    public Object addDo(@Param("userInfo") Sys_user user,@Param("rolesIds") String rolesIds, HttpServletRequest req) {
        try {
            if (Strings.isNotBlank(user.getLoginname())) {
                int num = sysUserService.count(Cnd.where("loginname", "=", Strings.trim(user.getLoginname())));
                if (num > 0) {
                    return Result.error("用户名已存在!");
                }
            }
            if (Strings.isNotBlank(user.getMobile())) {
                int num = sysUserService.count(Cnd.where("mobile", "=", Strings.trim(user.getMobile())));
                if (num > 0) {
                    return Result.error("手机号已存在!");
                }
            }
            if (Strings.isNotBlank(user.getEmail())) {
                int num = sysUserService.count(Cnd.where("email", "=", Strings.trim(user.getEmail())));
                if (num > 0) {
                    return Result.error("邮箱已存在!");
                }
            }
            String salt = R.UU32();
            user.setSalt(salt);
            user.setPassword(new Sha256Hash(user.getPassword(), ByteSource.Util.bytes(salt), 1024).toHex());
            user.setLoginPjax(true);
            user.setLoginCount(0);
            user.setOpBy(StringUtil.getPlatformUid());
            sysUserService.insert(user);
            if (Strings.isNotBlank(rolesIds)) {
                //先删除再增加
                sysUserService.execute(Sqls.create("delete from sys_user_role where userId='"+user.getId()+"'"));
                String[] ids = StringUtils.split(rolesIds, ",");
                for (String s : ids) {
                    sysUserService.insert("sys_user_role", org.nutz.dao.Chain.make("roleId", s).add("userId", user.getId()));
                }
            }
            sysUserService.clearCache();
            req.setAttribute("loginname", user.getLoginname());
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }
    @At("/thisEdit")
    @Ok("json")
    @RequiresAuthentication
    public Object thisEdit() {
        try {
            Sys_user user = sysUserService.fetchLinks(sysUserService.fetch(StringUtil.getPlatformUid()),null);
            return Result.success().addData(user);
        } catch (Exception e) {
            return Result.error();
        }
    }
    @At("/edit/?")
    @Ok("json")
    @RequiresAuthentication
    public Object edit(String id) {
        try {
            //Sys_user user = sysUserService.fetchLinks(sysUserService.fetchLinks(sysUserService.fetch(id), "unit"), "signatureImage");
            Sys_user user = sysUserService.fetchLinks(sysUserService.fetch(id),null);
            boolean bl = true;
            String unitid = user.getUnitid();
            String oldunitid = user.getUnitid();
            while (bl){
                List<Sys_unit> list= sysUnitService.listEntity(Sqls.create("select * from sys_unit where id = '"+oldunitid+"' and delFlag = 0 "));
                if(list.size()>0 && !list.get(0).getParentId().isEmpty()){
                    unitid=list.get(0).getParentId()+","+unitid;
                    oldunitid = list.get(0).getParentId();
                }else{
                    bl= false;
                }
            }
            List<NutMap> userRole = sysUserService.getUserRole(user.getId());
            String rolesIds = userRole.stream().map(t->t.get("roleId").toString()).collect(Collectors.joining(","));
            user.setRolesIds(rolesIds);
            user.setUnitid(unitid);
            return Result.success().addData(user);
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/detail/?")
    @Ok("json")
    @RequiresAuthentication
    public Object detail(String id) {
        try {
            Sys_user user = sysUserService.fetchLinks(sysUserService.fetch(id),null);
            return Result.success().addData(user);
        } catch (Exception e) {
            return Result.error();
        }
    }

    @Aop(TransAop.READ_COMMITTED)
    @AdaptBy(type = JsonAdaptor.class)
    @At
    @Ok("json")
    @RequiresAuthentication
    @SLog(tag = "修改用户", msg = "用户名:${args[1].getAttribute('loginname')}")
    public Object editDo(@Param("userInfo") Sys_user user,@Param("rolesIds") String rolesIds,  HttpServletRequest req) {
        try {
            user.setOpBy(StringUtil.getPlatformUid());
            user.setOpAt(Times.getTS());
            sysUserService.insertOrUpdate(user);
            if (Strings.isNotBlank(rolesIds)) {
                String[] ids = StringUtils.split(rolesIds, ",");
                //先删除再增加
                sysUserService.execute(Sqls.create("delete from sys_user_role where userId='"+user.getId()+"'"));
                for (String s : ids) {
                    sysUserService.insert("sys_user_role", org.nutz.dao.Chain.make("roleId", s).add("userId", user.getId()));
                }
            }
            sysUserService.deleteCache(user.getId());
            req.setAttribute("loginname", user.getLoginname());
            return Result.success();
        } catch (Exception e) {
            log.error(e);
            return Result.error();
        }
    }

    @At("/resetPwd/?")
    @Ok("json")
    @RequiresPermissions("sys.manager.user.edit")
    @SLog(tag = "重置密码", msg = "用户名:${args[1].getAttribute('loginname')}")
    public Object resetPwd(String id, HttpServletRequest req) {
        try {
            Sys_user user = sysUserService.fetch(id);
            String salt = R.UU32();
            String pwd = R.captchaNumber(6);
            String hashedPasswordBase64 = new Sha256Hash(pwd, ByteSource.Util.bytes(salt), 1024).toHex();
            sysUserService.update(Chain.make("salt", salt).add("password", hashedPasswordBase64), Cnd.where("id", "=", id));
            sysUserService.deleteCache(user.getId());
            req.setAttribute("loginname", user.getLoginname());
            return Result.success("操作成功！").addData(pwd);
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/delete/?")
    @Ok("json")
    @RequiresPermissions("sys.manager.user.delete")
    @SLog(tag = "删除用户", msg = "用户名:${args[1].getAttribute('loginname')}")
    public Object delete(String userId, HttpServletRequest req) {
        try {
            Sys_user user = sysUserService.fetch(userId);
            if ("superadmin".equals(user.getLoginname())) {
                return Result.error("system.not.allow");
            }
            sysUserService.deleteById(userId, user.getFileId());
            sysUserService.deleteCache(user.getId());
            req.setAttribute("loginname", user.getLoginname());
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/delete")
    @Ok("json")
    @RequiresPermissions("sys.manager.user.delete")
    @SLog(tag = "批量删除用户", msg = "用户ID:${args[2].getAttribute('ids')}")
    public Object deletes(@Param("ids") String[] userIds, @Param("fileIds") String[] fileIds, HttpServletRequest req) {
        try {
            Sys_user user = sysUserService.fetch(Cnd.where("loginname", "=", "superadmin"));
            StringBuilder sb = new StringBuilder();
            for (String s : userIds) {
                if (s.equals(user.getId())) {
                    return Result.error("system.not.allow");
                }
                sb.append(s).append(",");
            }
            sysUserService.deleteByIds(userIds, fileIds);
            sysUserService.clearCache();
            req.setAttribute("ids", sb.toString());
            return Result.success();
        } catch (Exception e) {
            log.error(e);
            return Result.error();
        }
    }

    @At("/enable/?")
    @Ok("json")
    @RequiresPermissions("sys.manager.user.edit")
    @SLog(tag = "启用用户", msg = "用户名:${args[1].getAttribute('loginname')}")
    public Object enable(String userId, HttpServletRequest req) {
        try {
            req.setAttribute("loginname", sysUserService.fetch(userId).getLoginname());
            sysUserService.update(Chain.make("disabled", false), Cnd.where("id", "=", userId));
            sysUserService.deleteCache(userId);
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/disable/?")
    @Ok("json")
    @RequiresPermissions("sys.manager.user.edit")
    @SLog(tag = "禁用用户", msg = "用户名:${args[1].getAttribute('loginname')}")
    public Object disable(String userId, HttpServletRequest req) {
        try {
            String loginname = sysUserService.fetch(userId).getLoginname();
            if ("superadmin".equals(loginname)) {
                return Result.error("system.not.allow");
            }
            req.setAttribute("loginname", loginname);
            sysUserService.update(Chain.make("disabled", true), Cnd.where("id", "=", userId));
            sysUserService.deleteCache(userId);
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/menu/?")
    @Ok("json")
    @RequiresPermissions("sys.manager.user")
    public Object menu(String id, @Param("pid") String pid, HttpServletRequest req) {
        try {
            List<Sys_menu> list = sysUserService.getRoleMenus(id, pid);
            List<NutMap> treeList = new ArrayList<>();
            for (Sys_menu unit : list) {
                if (!unit.isHasChildren() && sysUserService.hasChildren(id, unit.getId())) {
                    unit.setHasChildren(true);
                }
                NutMap map = Lang.obj2nutmap(unit);
                map.addv("expanded", false);
                map.addv("children", new ArrayList<>());
                treeList.add(map);
            }
            return Result.success().addData(treeList);
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At
    @Ok("json:{locked:'password|salt',ignoreNull:false}")
    @RequiresAuthentication
    public Object data(@Param("searchUnit") String searchUnit, @Param("searchName") String searchName, @Param("searchKeyword") String searchKeyword, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize, @Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy) {
        try {
            Cnd cnd = Cnd.NEW();
            if (Strings.isNotBlank(searchUnit)) {
                Sys_unit unit = sysUnitService.fetch(searchUnit);
                List<Sys_unit> units = sysUnitService.listEntity(Sqls.create("select * from sys_unit where path like '"+unit.getPath()+"%'"));
                String unitIds = "'"+units.stream().map(t->t.getId()).collect(Collectors.joining("','"))+"'";
                cnd.and("unitid", "in", unitIds);
                /*if (shiroUtil.hasRole("sysadmin")) {
                    cnd.and("unitid", "in", unitIds);
                } else {
                    Sys_user user = (Sys_user) shiroUtil.getPrincipal();
                    if (unit == null || !unit.getPath().startsWith(user.getUnit().getPath())) {
                        //防止有人越级访问
                        return Result.error("非法操作");
                    }
                    cnd.and("unitid", "in", unitIds);
                }*/
            }
            Sys_user user = StringUtil.getPlatformUser();
            SqlExpressionGroup group = new SqlExpressionGroup();
            for (Sys_role role : user.getRoles())
            {
                String dataScope = role.getDataScope();
                String units = "";
                if(StringUtils.isNotBlank(role.getUnits())){
                    units = "'"+ Arrays.stream(role.getUnits().split(",")).collect(Collectors.joining("','"))+"'";
                }
                if ("1".equals(dataScope))
                {
                    SqlExpression sqlExpression = Cnd.exp("1", "=", "1");
                    group.or(sqlExpression);
                }
                else if ("2".equals(dataScope))
                {
                    SqlExpression sqlExpression = Cnd.exp("unitid", "in", units);
                    group.or(sqlExpression);
                }
                else if ("3".equals(dataScope))
                {
                    SqlExpression sqlExpression = Cnd.exp("unitid", "=", user.getUnitid());
                    group.or(sqlExpression);
                }
                else if ("4".equals(dataScope))
                {
                    SqlExpression sqlExpression = Cnd.exp("id", "=", user.getId());
                    group.or(sqlExpression);
                }
            }
            cnd.and(group);
            if (Strings.isNotBlank(searchName) && Strings.isNotBlank(searchKeyword)) {
                cnd.and(searchName, "like", "%" + searchKeyword + "%");
            }
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                cnd.orderBy(pageOrderName, PageUtil.getOrder(pageOrderBy));
            }
            return Result.success().addData(sysUserService.listPageLinks(pageNumber, pageSize, cnd, "unit|roles"));
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At
    @Ok("void")
    @RequiresAuthentication
    public void export(@Param("searchUnit") String searchUnit, @Param("searchName") String searchName, @Param("searchKeyword") String searchKeyword, @Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy, HttpServletResponse response) {
        try {
            J4EConf j4eConf = J4EConf.from(Sys_user.class);
            List<J4EColumn> jcols = j4eConf.getColumns();
            for (J4EColumn j4eColumn : jcols) {
                if ("opBy".equals(j4eColumn.getFieldName()) || "opAt".equals(j4eColumn.getFieldName()) || "delFlag".equals(j4eColumn.getFieldName())) {
                    j4eColumn.setIgnore(true);
                }
            }
            Cnd cnd = Cnd.NEW();
            if (Strings.isNotBlank(searchUnit)) {
                Sys_unit unit = sysUnitService.fetch(searchUnit);
                List<Sys_unit> units = sysUnitService.listEntity(Sqls.create("select * from sys_unit where path like '"+unit.getPath()+"%'"));
                String unitIds = "'"+units.stream().map(t->t.getId()).collect(Collectors.joining("','"))+"'";
                cnd.and("unitid", "in", unitIds);
            }
            Sys_user user = StringUtil.getPlatformUser();
            SqlExpressionGroup group = new SqlExpressionGroup();
            for (Sys_role role : user.getRoles())
            {
                String dataScope = role.getDataScope();
                String units = "";
                if(StringUtils.isNotBlank(role.getUnits())){
                    units = "'"+ Arrays.stream(role.getUnits().split(",")).collect(Collectors.joining("','"))+"'";
                }
                if ("1".equals(dataScope))
                {
                    SqlExpression sqlExpression = Cnd.exp("1", "=", "1");
                    group.or(sqlExpression);
                }
                else if ("2".equals(dataScope))
                {
                    SqlExpression sqlExpression = Cnd.exp("unitid", "in", units);
                    group.or(sqlExpression);
                }
                else if ("3".equals(dataScope))
                {
                    SqlExpression sqlExpression = Cnd.exp("unitid", "=", user.getUnitid());
                    group.or(sqlExpression);
                }
                else if ("4".equals(dataScope))
                {
                    SqlExpression sqlExpression = Cnd.exp("id", "=", user.getId());
                    group.or(sqlExpression);
                }
            }
            cnd.and(group);
            if (Strings.isNotBlank(searchName) && Strings.isNotBlank(searchKeyword)) {
                cnd.and(searchName, "like", "%" + searchKeyword + "%");
            }
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                cnd.orderBy(pageOrderName, PageUtil.getOrder(pageOrderBy));
            }
            OutputStream out = response.getOutputStream();
            response.setHeader("content-type", "application/shlnd.ms-excel;charset=utf-8");
            response.setHeader("content-disposition", "attachment; filename=" + new String("用户信息".getBytes(), "ISO-8859-1") + ".xls");
            List<Sys_user> list = sysUserService.query(cnd, "unit");
            J4E.toExcel(out, list, j4eConf);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @At
    @Ok("beetl:/platform/sys/user/pass.html")
    @RequiresAuthentication
    public void pass() {

    }
    @At
    @Ok("beetl:/platform/sys/user/userDetail.html")
    @RequiresAuthentication
    public void userDetail() {

    }

    @At
    @Ok("beetl:/platform/sys/user/custom.html")
    @RequiresAuthentication
    public void custom() {

    }

    @At
    @Ok("beetl:/platform/sys/user/mode.html")
    @RequiresAuthentication
    public void mode() {

    }

    @At
    @Ok("json")
    @RequiresAuthentication
    public Object modeDo(@Param("mode") String mode, HttpServletRequest req) {
        try {
            sysUserService.update(Chain.make("loginPjax", "true".equals(mode)), Cnd.where("id", "=", StringUtil.getPlatformUid()));
            Subject subject = SecurityUtils.getSubject();
            Sys_user user = (Sys_user) subject.getPrincipal();
            if ("true".equals(mode)) {
                user.setLoginPjax(true);
            } else {
                user.setLoginPjax(false);
            }
            sysUserService.deleteCache(user.getId());
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }


    @At
    @Ok("json")
    @RequiresAuthentication
    public Object customDo(@Param("ids") String ids, HttpServletRequest req) {
        try {
            sysUserService.update(Chain.make("customMenu", ids), Cnd.where("id", "=", StringUtil.getPlatformUid()));
            Subject subject = SecurityUtils.getSubject();
            Sys_user user = (Sys_user) subject.getPrincipal();
            if (Strings.isNotBlank(ids)) {
                user.setCustomMenu(ids);
                user.setCustomMenus(sysMenuService.query(Cnd.where("id", "in", ids.split(","))));
            } else {
                user.setCustomMenu("");
                user.setCustomMenus(new ArrayList<>());
            }
            sysUserService.deleteCache(user.getId());
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At
    @Ok("json")
    @RequiresAuthentication
    public Object doChangePassword(@Param("oldPassword") String oldPassword, @Param("newPassword") String newPassword, HttpServletRequest req) {
        Subject subject = SecurityUtils.getSubject();
        Sys_user user = (Sys_user) subject.getPrincipal();
        String old = new Sha256Hash(oldPassword, user.getSalt(), 1024).toHex();
        if (old.equals(user.getPassword())) {
            String salt = R.UU32();
            String hashedPasswordBase64 = new Sha256Hash(newPassword, ByteSource.Util.bytes(salt), 1024).toHex();
            user.setSalt(salt);
            user.setPassword(hashedPasswordBase64);
            sysUserService.update(Chain.make("salt", salt).add("password", hashedPasswordBase64), Cnd.where("id", "=", user.getId()));
            sysUserService.deleteCache(user.getId());
            return Result.success();
        } else {
            return Result.error();
        }
    }

    @At
    @Ok("json")
    @RequiresAuthentication
    public Object getUserSAudit(@Param("userId") String userId) {
        if(StringUtils.isBlank(userId)){
            userId=StringUtil.getPlatformUid();
        }
        return Result.success(sysUserService.getUserSAudit(sysUserService.fetch(userId).getUnitid()));
    }
}
