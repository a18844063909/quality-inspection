package cn.wizzer.app.web.modules.controllers.platform.bus;

import cn.wizzer.app.bus.modules.models.BusCharacter;
import cn.wizzer.app.bus.modules.models.BusCompany;
import cn.wizzer.app.bus.modules.services.BusCharacterService;
import cn.wizzer.app.bus.modules.services.BusCompanyService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.PageUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@IocBean
@At("/platform/bus/character/")
public class BusCharacterController {

    private static final Log log = Logs.get();

    @Inject
    private BusCharacterService busCharacterService;
    @At("")
    @Ok("beetl:/platform/bus/configManagement/character/index.html")
    @RequiresPermissions("bus.character.index")
    public void index(HttpServletRequest req) {


    }
    @At
    @Ok("json:full")
    @RequiresPermissions("bus.character.data")
    public Object data( @Param("pageNumber") int pageNumber,@Param("pageSize") int pageSize) {
        try {
            String sql ="select * from bus_character where delFlag=0 and opBy='"+StringUtil.getPlatformUid()+"' order by opAt desc ";
            return Result.success().addData(busCharacterService.listPage(pageNumber, pageSize, Sqls.create(sql)));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At
    @Ok("json:full")
    @RequiresAuthentication
    public Object list() {
        try {
            String sql ="select * from bus_character where delFlag=0 order by opAt desc";
            return Result.success().addData(busCharacterService.listEntity(Sqls.create(sql)));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At
    @Ok("json")
    @RequiresPermissions("bus.character.add")
    @SLog(tag = "添加特殊字符", msg = "字符:${busCharacter.character}")
    public Object addDo(@Param("..") BusCharacter busCharacter, HttpServletRequest req) {
        try {
            busCharacterService.insert(busCharacter);
            return Result.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }

    @At
    @Ok("json")
    @RequiresPermissions("bus.character.edit")
    @SLog(tag = "修改特殊字符", msg = "字符:${busCharacter.character}")
    public Object editDo(@Param("..") BusCharacter busCharacter, HttpServletRequest req) {
        try {
            busCharacterService.update(busCharacter);
            return Result.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At("/detail/?")
    @Ok("json")
    @RequiresPermissions("bus.character.detail")
    public Object detail(String id, HttpServletRequest req) {
        try {
            BusCharacter busCharacter = busCharacterService.fetch(id);
            return Result.success(busCharacter);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At("/delete/?")
    @Ok("json")
    @RequiresPermissions("bus.character.delete")
    @SLog(tag = "删除特殊字符", msg = "特殊字符ID:${args[1].getAttribute('id')}")
    public Object delete(String id, HttpServletRequest req) {
        try {
            //逻辑删除
            busCharacterService.vDelete(id);
            return Result.success();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
}
