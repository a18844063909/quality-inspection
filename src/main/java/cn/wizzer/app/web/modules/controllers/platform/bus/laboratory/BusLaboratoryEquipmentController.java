package cn.wizzer.app.web.modules.controllers.platform.bus.laboratory;

import cn.wizzer.app.bus.modules.services.BusEquipmentInspectionInventoryService;
import cn.wizzer.app.bus.modules.services.BusLaboratoryEquipmentService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.framework.base.Result;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tcjinxiu.com
 * @time 2024-04-11 10:19:44
 *
 */
@IocBean
@At("/platform/bus/laboratory/equipment")
public class BusLaboratoryEquipmentController {
private static final Log log = Logs.get();
	@Inject
	BusLaboratoryEquipmentService busLaboratoryEquipmentService;

	@At({"/delete","/delete/?"})
	@Ok("json")
	@SLog(tag = "Delete", msg = "Delete:bus_laboratory_equipment")
	public Object delete(String id,@Param("ids") String[] ids, HttpServletRequest req) {
		try {
			if(ids!=null&&ids.length>0){
				busLaboratoryEquipmentService.delete(ids);
			}else{
				busLaboratoryEquipmentService.delete(id);
			}
			return Result.success("system.success");
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}

}
