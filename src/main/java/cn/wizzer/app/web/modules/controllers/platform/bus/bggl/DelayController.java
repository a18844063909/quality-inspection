package cn.wizzer.app.web.modules.controllers.platform.bus.bggl;

import cn.wizzer.app.bus.modules.models.YJbxx;
import cn.wizzer.app.bus.modules.models.YJbxxLcxx;
import cn.wizzer.app.bus.modules.services.YJbxxLcxxService;
import cn.wizzer.app.bus.modules.services.YJbxxService;
import cn.wizzer.app.sys.modules.services.SysDictService;
import cn.wizzer.app.sys.modules.services.SysUnitService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.DateUtil;
import cn.wizzer.app.web.commons.utils.ShiroUtil;
import cn.wizzer.framework.base.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Times;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.trans.Atom;
import org.nutz.trans.Trans;

import java.sql.Connection;


/**
 * 报告审核  虞秀斌  2019-11
 */
@IocBean
@At("/platform/bus/bggl")
public class DelayController {
    private static final Log log = Logs.get();
    @Inject
    private YJbxxService yjbxxService;
    @Inject
    private YJbxxLcxxService yjbxxlcxxservice;
    @Inject
    private SysUnitService sysUnitService;
    @Inject
    private SysDictService sysDictService;
    @Inject
    private SysUserService sysUserService;
    @Inject
    private ShiroUtil shiroUtil;

    @At
    @Ok("json")
    @RequiresPermissions("bus.bggl")
    @SLog(tag = "报告延期", msg = "样品:${args[0].ypbh}")
    public Object delayDo(@Param("..") YJbxx obj) {
        try {
            YJbxx jbxx = yjbxxService.fetch(obj.getId());
            jbxx.setSfyq("1");
            jbxx.setYqrq(DateUtil.getDateTime());
            jbxx.setWcqx(obj.getYqrq());
            yjbxxService.update(jbxx);
            yjbxxlcxxservice.clearCache();
            return Result.success("延期成功！");
        } catch (Exception e) {
            return Result.error("system.error");
        }
    }
}
