package cn.wizzer.app.web.modules.controllers.platform.bus.reports;

import cn.wizzer.app.bus.modules.services.YJbxxService;
import cn.wizzer.app.sys.modules.services.SysDictService;
import cn.wizzer.app.web.commons.utils.DateUtil;
import cn.wizzer.framework.base.Result;
import cn.wizzer.framework.page.Pagination;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.lang.util.NutMap;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


/**
 * 样品移交  虞秀斌  2019-11
 */
@IocBean
@At("/platform/bus/reports/acceptance")
public class AcceptanceCountController {
    private static final Log log = Logs.get();
    @Inject
    private YJbxxService yjbxxService;
    @Inject
    private SysDictService sysDictService;

    @At("/")
    @Ok("beetl:/platform/bus/reports/acceptance/index.html")
    @RequiresAuthentication
    public void index(HttpServletRequest req) {
        req.setAttribute("jylxMap", JSONObject.fromObject(sysDictService.getCodeMap("jylx")));
        req.setAttribute("lcztMap", JSONObject.fromObject(sysDictService.getCodeMap("lczt")));
    }

    @At
    @Ok("json")
    public Object allData(@Param("date") String date,@Param("type") String type) {
        try {
            String whereSql = "";
            /*if(StringUtils.isNotBlank(date)){
                whereSql+= " and year='"+date+"' ";
            }*/
            String dateSql = " substr(FROM_UNIXTIME(opAt),1,4) ";
            if(type.equals("month")){
                dateSql = " substr(FROM_UNIXTIME(opAt),1,7) ";
            }
            String sql = " select count(1) as count,year from (select "+dateSql+" as year from y_jbxx where delFlag=0) a where 1=1 "+whereSql+" group by year order by year desc ";

            return Result.success().addData(yjbxxService.list( Sqls.create(sql)));
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At
    @Ok("json")
    public Object oneData(@Param("date") String date,@Param("type") String type) {
        try {
            String whereSql = "";
            if(StringUtils.isNotBlank(date)){
                whereSql+= " and year='"+date+"' ";
            }
            String dateSql = " substr(FROM_UNIXTIME(opAt),1,4) ";
            if(type.equals("month")){
                dateSql = " substr(FROM_UNIXTIME(opAt),1,7) ";
            }
            //NutMap map = new NutMap();
            //样品登记数量
            String sql = " select count(1) as count,year,'样品登记数量' as name,'' as lczt from (select  "+dateSql+" as year from y_jbxx where delFlag=0) a where 1=1 "+whereSql+" group by year ";
            //map.addv("lcztCount",yjbxxService.list( Sqls.create(sql)));
            List list  = yjbxxService.list( Sqls.create(sql));
            //样品提交数量
            String sql0 = " select count(1) as count,year,'样品提交数量' as name,'0' as lczt from (select  "+dateSql+" as year from y_jbxx where delFlag=0 and lczt>0) a where 1=1 "+whereSql+" group by year";
            //map.addv("lczt0Count",yjbxxService.list( Sqls.create(sql0)));
            list.addAll(yjbxxService.list( Sqls.create(sql0)));

            //任务分配数量
            String sql1 = " select count(1) as count,year,'任务分配数量' as name,'1' as lczt  from (select  "+dateSql+" as year from y_jbxx where delFlag=0 and lczt>1) a where 1=1 "+whereSql+" group by year";
            //map.addv("lczt1Count",yjbxxService.list( Sqls.create(sql1)));
            list.addAll(yjbxxService.list( Sqls.create(sql1)));

            //报告编制数量
            String sql2 = " select count(1) as count,year,'报告编制数量' as name,'2' as lczt from (select  "+dateSql+" as year from y_jbxx where delFlag=0 and lczt>2) a where 1=1 "+whereSql+" group by year";
            //map.addv("lczt2Count",yjbxxService.list( Sqls.create(sql2)));
            list.addAll(yjbxxService.list( Sqls.create(sql2)));

            //报告审核通过数量
            String sql6 = " select count(1) as count,year,'审核通过数量' as name,'6' as lczt from (select  "+dateSql+" as year from y_jbxx where delFlag=0 and lczt>6) a where 1=1 "+whereSql+" group by year";
            //map.addv("lczt6Count",yjbxxService.list( Sqls.create(sql6)));
            list.addAll(yjbxxService.list( Sqls.create(sql6)));

            //报告批准通过数量
            String sql8 = " select count(1) as count,year,'批准通过数量' as name,'8' as lczt from (select  "+dateSql+" as year from y_jbxx where delFlag=0 and lczt>8) a where 1=1 "+whereSql+" group by year";
            //map.addv("lczt8Count",yjbxxService.list( Sqls.create(sql8)));
            list.addAll(yjbxxService.list( Sqls.create(sql8)));


            //报告打印数量
            String sqldy = " select count(1) as count,year,'已打印数量' as name,'99' as lczt from (select  "+dateSql+" as year from y_jbxx where delFlag=0 and lczt>8 and dyzt=1) a where 1=1 "+whereSql+" group by year";
            //map.addv("lczt8Count",yjbxxService.list( Sqls.create(sql8)));
            list.addAll(yjbxxService.list( Sqls.create(sqldy)));

            return Result.success().addData(list);
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At
    @Ok("json")
    public Object jbxxData(@Param("date") String date,@Param("dateType") String dateType,@Param("lczt") String lczt,@Param("searchKeyword") String searchKeyword,@Param("userid") String userid,@Param("pageNumber") int pageNumber,
                       @Param("pageSize") int pageSize, @Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy) {
        try {

            String sql ="select * from y_jbxx  where 1=1 and delFlag=0 ";//nextsprIds

            if(StringUtils.isNotBlank(date)){
                if(dateType.equals("month")){
                    sql += " and substr(FROM_UNIXTIME(opAt),1,7) ='"+date+"'";
                }else{
                    sql += " and  substr(FROM_UNIXTIME(opAt),1,4) ='"+date+"'";
                }
            }
            if(StringUtils.isNotBlank(lczt)){
                if(lczt.equals("99")){
                    sql+=" and lczt>8 and dyzt=1 ";
                }else{
                    sql+= " and  lczt>"+lczt+" ";
                }
            }
            if(StringUtils.isNotBlank(searchKeyword)){
                sql+= " and (ypbh like '%"+searchKeyword+"%' or ypmc like '%"+searchKeyword+"%')";
            }
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                if ("ascending".equalsIgnoreCase(pageOrderBy)) {
                    sql+= " order by "+pageOrderName+" asc,opAt desc ";
                } else {
                    sql+= " order by "+pageOrderName+" desc,opAt desc ";
                }
            }else{
                sql+= " order by opAt desc ";
            }
            Pagination pagination  = yjbxxService.listPageMap(pageNumber,pageSize, Sqls.create(sql));
            return Result.success().addData(pagination);
        } catch (Exception e) {
            return Result.error();
        }
    }


}
