package cn.wizzer.app.web.modules.controllers.platform.sys;

import cn.wizzer.app.bus.modules.services.YJbxxService;
import cn.wizzer.app.sys.modules.models.Sys_menu;
import cn.wizzer.app.sys.modules.services.SysMenuService;
import cn.wizzer.app.web.commons.base.Globals;
import cn.wizzer.app.web.commons.utils.DateUtil;
import cn.wizzer.app.web.commons.utils.ShiroUtil;
import cn.wizzer.framework.base.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.json.JSONObject;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wizzer on 2016/6/23.
 */
@IocBean
@At("/platform/home")
public class SysHomeController {
    private static final Log log = Logs.get();
    @Inject
    private SysMenuService sysMenuService;
    @Inject
    private ShiroUtil shiroUtil;
    @Inject
    private YJbxxService yjbxxService;

    @At("")
    @Ok("beetl:/platform/sys/home.html")
    @RequiresAuthentication
    public void home(HttpSession session,HttpServletRequest req) {

        req.setAttribute("nowDay", DateUtil.getDate());


    }

    @At
    @Ok("json")
    public Object countData(@Param("date") String date) {
        try {
            List<JSONObject> data = new ArrayList<JSONObject>();
            //NutMap map = new NutMap();
            //样品提交数量
            String sql0 = "select  count(1) as count,'"+date+"' as date,'今日样品提交' as name,0 as dyzt from y_jbxx where delFlag=0 and lczt>0 and substr(FROM_UNIXTIME(opAt),1,10)='"+date+"'";
            List list  =yjbxxService.list( Sqls.create(sql0));
            //样品提交数量
            String sql2 = "select  count(1) as count,'"+date.substring(0,7)+"' as date,'本月样品提交' as name,0 as dyzt from y_jbxx where delFlag=0 and lczt>0 and substr(FROM_UNIXTIME(opAt),1,7)='"+date.substring(0,7)+"'";
            list.addAll(yjbxxService.list( Sqls.create(sql2)));
            //样品提交数量
            String sql4 = "select  count(1) as count,'"+date.substring(0,4)+"' as date,'今年样品提交' as name,0 as dyzt from y_jbxx where delFlag=0 and lczt>0 and substr(FROM_UNIXTIME(opAt),1,4)='"+date.substring(0,4)+"'";
            list.addAll(yjbxxService.list( Sqls.create(sql4)));
            //样品提交数量
            String sql6 = "select  count(1) as count,'all' as date,'累计样品提交' as name,0 as dyzt from y_jbxx where delFlag=0 and lczt>0 ";
            list.addAll(yjbxxService.list( Sqls.create(sql6)));


            //报告已打印数量
            String sql1 = "select  count(1) as count,'"+date+"' as date,'今日报告打印' as name,1 as dyzt from y_jbxx where delFlag=0 and lczt=13 and dyzt=1  and substr(dysj,1,10)='"+date+"'";
            list.addAll(yjbxxService.list( Sqls.create(sql1)));
            //报告已打印数量
            String sql3 = "select  count(1) as count,'"+date.substring(0,7)+"' as date,'本月报告打印' as name,1 as dyzt from y_jbxx where delFlag=0 and lczt=13 and dyzt=1  and substr(dysj,1,7)='"+date.substring(0,7)+"'";
            list.addAll(yjbxxService.list( Sqls.create(sql3)));
            //报告已打印数量
            String sql5 = "select count(1) as count,'"+date.substring(0,4)+"' as date,'今年报告打印' as name,1 as dyzt  from y_jbxx where delFlag=0 and lczt=13 and dyzt=1  and substr(dysj,1,4)='"+date.substring(0,4)+"'";
            list.addAll(yjbxxService.list( Sqls.create(sql5)));
            //报告已打印数量
            String sql7 = "select count(1) as count,'all' as date,'累计报告打印' as name,1 as dyzt  from y_jbxx where delFlag=0 and lczt=13  and dyzt=1 ";
            list.addAll(yjbxxService.list( Sqls.create(sql7)));

            return Result.success().addData(list);
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At
    @Ok("json")
    public Object countTypeData(@Param("date") String date,@Param("dyzt") String dyzt) {
        try {
            //NutMap map = new NutMap();
            //样品提交数量
            String sqlWhere  = "";
            if(StringUtils.isNotBlank(dyzt)&&dyzt.equals("1")){
                sqlWhere += " and lczt>8  and dyzt=1 ";
                if(StringUtils.isNotBlank(date)&&!date.equals("all")){
                    sqlWhere += " and substr(dysj,1,"+date.length()+")='"+date+"' ";
                }
            }else{
                sqlWhere += " and lczt>0 ";
                if(StringUtils.isNotBlank(date)&&!date.equals("all")){
                    sqlWhere += " and substr(FROM_UNIXTIME(opAt),1,"+date.length()+")='"+date+"' ";
                }
            }
            String sql = "select a.count,a.zh,d.name from (select count(1) as count,zh from y_jbxx " +
                    "where delFlag =0 and zh is not null and zh !='' "+sqlWhere+" group by zh  ) a left join sys_dict d on d.id = a.zh order by d.opAt ";
            List list  =yjbxxService.list( Sqls.create(sql));
                       return Result.success().addData(list);
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At
    @Ok("beetl:/platform/sys/left.html")
    @RequiresAuthentication
    public void left(@Param("url") String url, HttpServletRequest req) {
        String path = "";
        String perpath = "";
        url = Strings.sNull(url).trim();
        if (!Strings.isBlank(Globals.AppBase)) {
            url = Strings.sBlank(url).substring(Globals.AppBase.length());
        }
        if (Strings.sBlank(url).indexOf("?") > 0)
            url = url.substring(0, url.indexOf("?"));
        Sys_menu menu = sysMenuService.getLeftMenu(url);
        if (menu != null) {
            if (menu.getPath().length() >= 8) {
                path = menu.getPath().substring(0, 8);
                perpath = menu.getPath().substring(0, 4);
            }
            req.setAttribute("mpath", menu.getPath());
        }
        req.setAttribute("path", path);
        req.setAttribute("perpath", perpath);
    }

    @At
    @Ok("beetl:/platform/sys/left.html")
    @RequiresAuthentication
    public void path(@Param("url") String url, HttpServletRequest req) {
        url = Strings.sNull(url).trim();
        if (Strings.sBlank(url).indexOf("//") > 0) {
            String[] u = url.split("//");
            String s = u[1].substring(u[1].indexOf("/"));
            if (Strings.sBlank(s).indexOf("?") > 0)
                s = s.substring(0, s.indexOf("?"));
            if (!Strings.isBlank(Globals.AppBase)) {
                s = s.substring(Globals.AppBase.length());
            }
            String[] urls = s.split("/");
            List<String> list = new ArrayList<>();
            if (urls.length > 5) {
                list.add("/" + urls[1] + "/" + urls[2] + "/" + urls[3] + "/" + urls[4] + "/" + urls[5]);
                list.add("/" + urls[1] + "/" + urls[2] + "/" + urls[3] + "/" + urls[4]);
                list.add("/" + urls[1] + "/" + urls[2] + "/" + urls[3]);
                list.add("/" + urls[1] + "/" + urls[2]);
                list.add("/" + urls[1]);
            } else if (urls.length == 5) {
                list.add("/" + urls[1] + "/" + urls[2] + "/" + urls[3] + "/" + urls[4]);
                list.add("/" + urls[1] + "/" + urls[2] + "/" + urls[3]);
                list.add("/" + urls[1] + "/" + urls[2]);
                list.add("/" + urls[1]);
            } else if (urls.length == 4) {
                list.add("/" + urls[1] + "/" + urls[2] + "/" + urls[3]);
                list.add("/" + urls[1] + "/" + urls[2]);
                list.add("/" + urls[1]);
            } else if (urls.length == 3) {
                list.add("/" + urls[1] + "/" + urls[2]);
                list.add("/" + urls[1]);
            } else if (urls.length == 2) {
                list.add("/" + urls[1]);
            } else list.add(url);
            String path = "";
            String perpath = "";
            Sys_menu menu = sysMenuService.getLeftPathMenu(list);
            if (menu != null) {
                if (menu.getPath().length() >= 8) {
                    path = menu.getPath().substring(0, 8);
                    perpath = menu.getPath().substring(0, 4);
                }
                req.setAttribute("mpath", menu.getPath());
            }
            req.setAttribute("path", path);
            req.setAttribute("perpath", perpath);
        }
    }

    @At("/403")
    @Ok("re")
    public Object error403() {
        if (shiroUtil.isAuthenticated()) {
            return "beetl:/platform/sys/403.html";
        } else {
            return ">>:/error/404.html";
        }
    }

    @At("/404")
    @Ok("re")
    public Object error404() {
        if (shiroUtil.isAuthenticated()) {
            return "beetl:/platform/sys/404.html";
        } else {
            return ">>:/error/404.html";
        }
    }

    @At("/500")
    @Ok("re")
    public Object error500() {
        if (shiroUtil.isAuthenticated()) {
            return "beetl:/platform/sys/500.html";
        } else {
            return ">>:/error/500.html";
        }
    }

    @At(value = {"/", "/index"}, top = true)
    @Ok(">>:/sysadmin")
    public void index() {

    }
}
