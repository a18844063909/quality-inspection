package cn.wizzer.app.web.modules.controllers.platform.sys;

import cn.wizzer.app.sys.modules.models.UreportFileTbl;
import cn.wizzer.app.sys.modules.services.UreportFileTblService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.PageUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.lang.Times;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

/**
 * Created by wizzer on 2016/6/28.
 */
@IocBean
@At("/platform/sys/ureportM")
public class UreportFileTblController {
    private static final Log log = Logs.get();
    @Inject
    private UreportFileTblService ureportFileTblService;

    @At("")
    @Ok("beetl:/platform/sys/report/index.html")
    @RequiresPermissions("sys.ureportM")
    public void index() {

    }

    @At
    @Ok("json")
    @RequiresPermissions("sys.ureportM.add")
    @SLog(tag = "添加报表", msg = "添加报表:${ureport.name}")
    public Object addDo(@Param("..") UreportFileTbl ureport) {
        try {
            ureport.setOpBy(StringUtil.getPlatformUid());
            ureport.setOpAt(Times.getTS());
            ureportFileTblService.insert(ureport);
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/edit/?")
    @Ok("json")
    @RequiresPermissions("sys.ureportM.edit")
    public Object edit(String id) {
        try {
            return Result.success().addData(ureportFileTblService.fetch(id));
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At
    @Ok("json")
    @RequiresPermissions("sys.ureportM.edit")
    @SLog(tag = "修改报表", msg = "修改报表:${ureport.name}")
    public Object editDo(@Param("..") UreportFileTbl ureport) {
        try {
            ureport.setOpBy(StringUtil.getPlatformUid());
            ureport.setOpAt(Times.getTS());
            ureportFileTblService.updateIgnoreNull(ureport);
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At("/delete/?")
    @Ok("json")
    @RequiresPermissions("sys.ureportM.delete")
    @SLog(tag = "删除报表", msg = "报表:${id}")
    public Object delete(String id) {
        try {
            ureportFileTblService.vDelete(id);
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At
    @Ok("json:full")
    @RequiresPermissions("sys.ureportM")
    public Object data(@Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize, @Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy) {
        try {
            Cnd cnd = Cnd.NEW();
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                cnd.orderBy(pageOrderName, PageUtil.getOrder(pageOrderBy));
            }
            return Result.success().addData(ureportFileTblService.listPage(pageNumber, pageSize, cnd));
        } catch (Exception e) {
            return Result.error();
        }
    }
}
