package cn.wizzer.app.web.modules.controllers.platform.bus.reports;

import cn.wizzer.app.bus.modules.services.BusinessFileInfoService;
import cn.wizzer.app.bus.modules.services.YJbxxLzxxService;
import cn.wizzer.app.bus.modules.services.YJbxxService;
import cn.wizzer.app.sys.modules.services.SysDictService;
import cn.wizzer.app.sys.modules.services.SysUnitService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.utils.DateUtil;
import cn.wizzer.framework.base.Result;
import cn.wizzer.framework.page.Pagination;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;


/**
 * 样品移交  虞秀斌  2019-11
 */
@IocBean
@At("/platform/bus/reports/promptness")
public class PromptnessCountController {
    private static final Log log = Logs.get();
    @Inject
    private YJbxxService yjbxxService;
    @Inject
    private SysDictService sysDictService;

    @At("/")
    @Ok("beetl:/platform/bus/reports/promptness/index.html")
    @RequiresAuthentication
    public void index(HttpServletRequest req) {
        req.setAttribute("jylxMap", JSONObject.fromObject(sysDictService.getCodeMap("jylx")));
        req.setAttribute("lcztMap", JSONObject.fromObject(sysDictService.getCodeMap("lczt")));

        req.setAttribute("startDate", DateUtil.getMonthBegin());
        req.setAttribute("endDate", DateUtil.getMonthEnd());


    }

    @At
    @Ok("json")
    @RequiresAuthentication
    public Object data(@Param("startDate") String startDate,@Param("endDate") String endDate,@Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy) {
        try {
            String dateSql = "";
            if(StringUtils.isNotBlank(startDate)){
                dateSql+= " and l2.opAt>="+DateUtil.getLongTime(startDate)+" ";
            }
            if(StringUtils.isNotBlank(endDate)){
                dateSql+= " and l2.opAt<="+DateUtil.getLongTime(endDate)+" ";
            }
            String sql = "select * from (select ur.id,ur.username ,ut.name,ur.unitid,case when s.jsnum then s.jsnum else 0 end jsnum," +
                    " case when s.num then s.num else 0 end num,case when s.jsl then s.jsl else 0 end jsl  from sys_user ur left join  " +
                    " (select id, sum(sfwc) jsnum,count(1) num ,ROUND(sum(sfwc)/count(1)*100, 2)  jsl from (SELECT " +
                    " ul.id, case when j.wcqx is not null and j.wcqx !='' and substr(FROM_UNIXTIME( l2.opAt ),1,10 )>j.wcqx then 0 else 1 end as sfwc " +
                    " FROM y_jbxx j LEFT JOIN y_jbxx_lcxx l ON j.id = l.jbxxId  and l.lczt = 2 and l.delFlag=0 " +
                    " LEFT JOIN y_jbxx_lcxx l2 ON j.id = l2.jbxxId  AND l2.lczt = j.lczt  AND l2.delFlag = 0  " +
                    " left join sys_user ul on l.nextsprIds like concat('%',ul.id  ,'%') where j.delFlag=0 and j.lczt in (9,13) "+dateSql+" ) x group by id )s on s.id = ur.id  " +
                    " left join sys_unit ut on ur.unitid = ut.id  where ur.disabled=0 and ur.delFlag=0 and  ut.sfjybm = 1 ) h";
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                if ("ascending".equalsIgnoreCase(pageOrderBy)) {
                    sql+= " order by "+pageOrderName+" asc";
                } else {
                    sql+= " order by "+pageOrderName+" desc ";
                }
            }else{
                sql+= " order by unitid desc,jsl desc  ";
            }
            return Result.success().addData(yjbxxService.list( Sqls.create(sql)));
        } catch (Exception e) {
            return Result.error();
        }
    }
    @At
    @Ok("json")
    @RequiresAuthentication
    public Object jbxxData(@Param("startDate") String startDate,@Param("endDate") String endDate,@Param("searchKeyword") String searchKeyword,@Param("sfwc") String sfwc,@Param("userid") String userid,@Param("pageNumber") int pageNumber,
                       @Param("pageSize") int pageSize, @Param("yppageOrderName") String pageOrderName, @Param("yppageOrderBy") String pageOrderBy) {
        try {

            String dateSql = "";
            if(StringUtils.isNotBlank(startDate)){
                dateSql+= " and l2.opAt>="+DateUtil.getLongTime(startDate)+" ";
            }
            if(StringUtils.isNotBlank(endDate)){
                dateSql+= " and l2.opAt<="+DateUtil.getLongTime(endDate)+" ";
            }
            String sql ="select *,case when sfwc=1 then '否' else '是' end as  sfcq from (SELECT  j.*,FROM_UNIXTIME(l2.opAt) as wcsj, " +
                    " case when j.wcqx is not null and j.wcqx !='' and substr(FROM_UNIXTIME( l2.opAt ),1,10 )>j.wcqx then 0 else 1 end as sfwc  FROM y_jbxx j " +
                    " LEFT JOIN y_jbxx_lcxx l ON j.id = l.jbxxId  and l.lczt = 2 and l.delFlag=0 " +
                    " LEFT JOIN y_jbxx_lcxx l2 ON j.id = l2.jbxxId  and l2.lczt = j.lczt and l2.delFlag=0  " +
                    " where  j.delFlag=0 and j.lczt in (9,13) "+dateSql+" and l.nextsprIds like '%"+userid+"%')aa " +
                    " where 1=1 ";
            if(StringUtils.isNotBlank(sfwc)){
                sql+= " and aa.sfwc='"+sfwc+"'";
            }
            if(StringUtils.isNotBlank(searchKeyword)){
                sql+= " and (aa.ypbh like '%"+searchKeyword+"%' or aa.ypmc like '%"+searchKeyword+"%')";
            }
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                if ("ascending".equalsIgnoreCase(pageOrderBy)) {
                    sql+= " order by "+pageOrderName+" asc ";
                } else {
                    sql+= " order by "+pageOrderName+" desc ";
                }
            }else{
                sql+= " order by wcsj desc ";
            }
            Pagination pagination  = yjbxxService.listPageMap(pageNumber,pageSize, Sqls.create(sql));
            return Result.success().addData(pagination);
        } catch (Exception e) {
            return Result.error();
        }
    }


}
