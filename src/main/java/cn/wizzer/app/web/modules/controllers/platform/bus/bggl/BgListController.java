package cn.wizzer.app.web.modules.controllers.platform.bus.bggl;

import cn.wizzer.app.bus.modules.models.BusinessFileInfo;
import cn.wizzer.app.bus.modules.models.YJbxx;
import cn.wizzer.app.bus.modules.models.YJbxxLcxx;
import cn.wizzer.app.bus.modules.services.YJbxxLcxxService;
import cn.wizzer.app.bus.modules.services.YJbxxService;
import cn.wizzer.app.sys.modules.models.Sys_role;
import cn.wizzer.app.sys.modules.models.Sys_user;
import cn.wizzer.app.sys.modules.services.SysDictService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.doc.DocUtils;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.ShiroUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.trans.Atom;
import org.nutz.trans.Trans;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 报告管理  虞秀斌  2019-11
 */
@IocBean
@At("/platform/bus/bggl")
public class BgListController {
    private static final Log log = Logs.get();
    @Inject
    private YJbxxService yjbxxService;
    @Inject
    private YJbxxLcxxService yjbxxlcxxservice;
    @Inject
    private SysDictService sysDictService;
    @Inject
    private SysUserService sysUserService;
    @Inject
    private ShiroUtil shiroUtil;
    @At("/?")
    @Ok("beetl:/platform/bus/bggl/list.html")
    @RequiresAuthentication
    public void index(String operation,HttpServletRequest req) {
        req.setAttribute("jylxMap", JSONObject.fromObject(sysDictService.getCodeMap("jylx")));
        String lczt="";
        String menuName="这是什么菜单？";
        if(operation.equals("bz")){
            lczt="2,4,6,8,12";
            menuName="报告编制";
        }
        if(operation.equals("sh")){
            lczt="5";
            menuName="报告审核";
        }
        if(operation.equals("pz")){
            lczt="7";
            menuName="报告批准";
        }
        /*if(operation.equals("js")){
            lczt="9";
            menuName="报告接收";
        }
        if(operation.equals("gd")){
            lczt="10";
            menuName="报告归档";
        }*/
        if(operation.equals("js")){
            lczt="9,13";
            menuName="报告解锁";
        }
        if(operation.equals("yq")){
            lczt="1,2,3,4,5,6,7,8,9,12,13";
            menuName="报告延期";
        }
        if(operation.equals("dy")){
            lczt="9,13";
            menuName="报告打印";
        }

        req.setAttribute("lczt",lczt);
        req.setAttribute("menuName",menuName);
        req.setAttribute("operation",operation);
        req.setAttribute("userId",shiroUtil.getPrincipalProperty("id").toString());
        req.setAttribute("username",shiroUtil.getPrincipalProperty("username").toString());
        req.setAttribute("lcztMap", JSONObject.fromObject(sysDictService.getCodeMap("lczt")));
        req.setAttribute("userOptions", JSONArray.fromObject(sysUserService.getUserAudit("2")));

    }

    @At
    @Ok("json:{locked:'password|salt',ignoreNull:false}")
    @RequiresPermissions("bus.bggl")
    public Object data(@Param("searchKeyword") String searchKeyword,@Param("lczt") String lczt, @Param("dyzt") String dyzt,@Param("operation") String operation,
                       @Param("startDate") String startDate,@Param("endDate") String endDate,@Param("pageNumber") int pageNumber,
                       @Param("pageSize") int pageSize, @Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy) {
        try {
            //and nextsprIds like '%"+StringUtil.getPlatformUid()+"%'
            String orderSql = "";
            String lcxxSql = "";
            if(operation.equals("bz")){
                lcxxSql +=" and l.nextsprIds like '%"+StringUtil.getPlatformUid()+"%'";
                orderSql = "tjsj";
            }
            String shsql ="";
            if(operation.equals("sh")){
                shsql =" ,(select FROM_UNIXTIME(max(opAt)) from y_jbxx_lcxx where jbxxId = jbxx.id and lczt=5 and delFlag=0) as bgtjsj ";
                lcxxSql +=" and l.nextsprIds like '%"+StringUtil.getPlatformUid()+"%'";
                orderSql = "bgtjsj";
            }
            String pzsql ="";
            if(operation.equals("pz")){
                pzsql =" ,(select FROM_UNIXTIME(max(opAt)) from y_jbxx_lcxx where jbxxId = jbxx.id and lczt=7  and delFlag=0) as bgtjsj ";
                lcxxSql +=" and l.nextsprIds like '%"+StringUtil.getPlatformUid()+"%'";
                orderSql = "bgtjsj";
            }
            String yqsql ="";
            if(operation.equals("yq")){
                yqsql =" ,(select FROM_UNIXTIME(max(opAt)) from y_jbxx_lcxx where jbxxId = jbxx.id and delFlag=0) as czsj ";
                orderSql = "czsj";
            }
            if(operation.equals("js")){
                lcxxSql +=" and jl2.opBy like '%"+StringUtil.getPlatformUid()+"%'";
                orderSql = "pzsj";
            }
            String sql ="SELECT jbxx.id,jbxx.lzzt,jbxx.lczt, jbxx.ypbh,jbxx.bgbh, jbxx.ypmc, jbxx.jylx, jbxx.jyks, jbxx.wtdw, jbxx.ggxh, jbxx.scrqpc, jbxx.sb,jbxx.wcqx, '' AS sysj, jbxx.jyfy, " +
                    " jbxx.dyzt,jbxx.dysj,jbxx.dycs,FROM_UNIXTIME(jbxx.opAt) as tjsj, FROM_UNIXTIME(jl2.opAt) as pzsj,jl.nextSpr,jbxx.yqrq " +shsql+pzsql+yqsql+
                    " FROM y_jbxx jbxx  " +
                    " left join y_jbxx_lcxx l on l.jbxxId=jbxx.id and l.lczt=jbxx.lczt AND l.delflag = 0" +
                    " LEFT JOIN y_jbxx_lcxx jl on jl.jbxxId=jbxx.id and jl.lczt=2 and jl.delFlag=0  " +
                    " LEFT JOIN y_jbxx_lcxx jl2 on jl2.jbxxId=jbxx.id and jl2.lczt=9 and jl2.delFlag=0  " +
                    "where 1=1 and jbxx.lczt in ("+lczt+") and jbxx.delflag = 0  "+lcxxSql+" ";
            if(operation.equals("dy")){
                sql +=StringUtil.dataScopeFilter("jbxx");
                orderSql = "pzsj";
            }
            if(operation.equals("js")){
                //sql+=StringUtil.dataScopeFilterJyksBh("jbxx","jyksbh");
               // sql +=StringUtil.dataScopeFilter("jbxx");
            }
            if(operation.equals("yq")){
                sql+=StringUtil.dataScopeFilterJyksBh("jbxx","jyksbh");
            }
            if (Strings.isNotBlank(searchKeyword)) {
                sql+= " and (jbxx.ypbh like '%"+searchKeyword+"%' or jbxx.ypmc like '%"+searchKeyword+"%') ";
            }
            if (Strings.isNotBlank(dyzt)) {
                if(dyzt.equals("1")) {
                    sql += " and jbxx.dyzt = '" + dyzt + "' ";
                    orderSql = "dysj";

                    if (Strings.isNotBlank(startDate)) {
                        sql+= " and dysj >= '"+startDate+"'";
                    }
                    if (Strings.isNotBlank(endDate)) {
                        sql+= " and dysj <= '"+endDate+"'";
                    }
                }else if(dyzt.equals("0"))
                    sql+= " and jbxx.dyzt !=1 ";
            }
            sql = "select * from ("+sql+") as x ";
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                if ("ascending".equalsIgnoreCase(pageOrderBy)) {
                    sql+= " order by "+pageOrderName+" asc,"+orderSql+" desc ";
                } else {
                    sql+= " order by "+pageOrderName+" desc,"+orderSql+" desc ";
                }
            }else{
                sql+= " order by "+orderSql+" desc ";
            }
            return Result.success().addData(yjbxxService.listPage(pageNumber,pageSize, Sqls.create(sql)));
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error();
        }
    }



}
