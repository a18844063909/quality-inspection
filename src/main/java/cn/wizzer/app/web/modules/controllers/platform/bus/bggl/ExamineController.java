package cn.wizzer.app.web.modules.controllers.platform.bus.bggl;

import cn.wizzer.app.bus.modules.models.BusinessFileInfo;
import cn.wizzer.app.bus.modules.models.YJbxx;
import cn.wizzer.app.bus.modules.models.YJbxxLcxx;
import cn.wizzer.app.bus.modules.services.BusinessFileInfoService;
import cn.wizzer.app.bus.modules.services.YJbxxLcxxService;
import cn.wizzer.app.bus.modules.services.YJbxxService;
import cn.wizzer.app.sys.modules.models.Sys_user;
import cn.wizzer.app.sys.modules.services.SysDictService;
import cn.wizzer.app.sys.modules.services.SysUnitService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.base.Globals;
import cn.wizzer.app.web.commons.doc.DocUtils;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.ShiroUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.app.web.modules.controllers.open.file.UploadController;
import cn.wizzer.framework.base.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
import org.nutz.trans.Atom;
import org.nutz.trans.Trans;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;


/**
 * 报告审核  虞秀斌  2019-11
 */
@IocBean
@At("/platform/bus/bggl")
public class ExamineController {
    private static final Log log = Logs.get();
    @Inject
    private YJbxxService yjbxxService;
    @Inject
    private YJbxxLcxxService yjbxxlcxxservice;
    @Inject
    private SysUnitService sysUnitService;
    @Inject
    private SysDictService sysDictService;
    @Inject
    private SysUserService sysUserService;
    @Inject
    private ShiroUtil shiroUtil;
    @Inject
    private BusinessFileInfoService businessFileInfoService;

    @At
    @Ok("json")
    @RequiresPermissions("bus.bggl")
    @SLog(tag = "报告审核", msg = "样品:${args[0].ypbh}")
    public Object examineDo(@Param("..") YJbxxLcxx obj) {
        try {
            YJbxx jbxx = yjbxxService.fetch(obj.getJbxxId());
            //获取报告
            BusinessFileInfo report = businessFileInfoService.fetch(Cnd.where("delFlag", "=", "0").and("fileName", "=", obj.getId()));
            String uploadPath = this.getUploadPathConfig();
            if(obj.getLczt().equals("7")) {
                Map<String, String> map = new HashMap<String, String>();
                //map.put("P2_shr", "D:/office online/test/dzh3.png");
                /*BusinessFileInfo signatureImage = (BusinessFileInfo)shiroUtil.getPrincipalProperty("signatureImage");
                if(signatureImage!=null){
                    map.put("P2_shr", uploadPath+signatureImage.getFilePath());
                }*/


                Sys_user slrUser = sysUserService.fetch(StringUtil.getPlatformUid());
                String fileId = slrUser.getFileId();
                if(!StringUtils.isEmpty(fileId)){
                    BusinessFileInfo signatureImage = businessFileInfoService.fetch(fileId);
                    map.put("P2_shr", uploadPath+signatureImage.getFilePath());
                }
                String newReport = uploadPath+report.getFilePath();
                DocUtils.FileToFile2(newReport, newReport, map, null);
            }else if(obj.getLczt().equals("6")) {
                Cnd cnd = Cnd.NEW();
                cnd.and("jbxxid","=",obj.getJbxxId());
                cnd.and("lczt","=","2");
                cnd.and("delflag","=","0");
                YJbxxLcxx lcxx = yjbxxlcxxservice.fetch(cnd);
                obj.setNextsprIds(lcxx.getNextsprIds());
                obj.setNextspr(lcxx.getNextspr());
            }
            jbxx.setLczt(obj.getLczt());
            Trans.exec(Connection.TRANSACTION_SERIALIZABLE, new Atom() {
                public void run() {
                    yjbxxlcxxservice.execute(Sqls.create("update y_jbxx_lcxx set delflag=1 where lczt>="+jbxx.getLczt()+" and jbxxid='"+jbxx.getId()+"'"));
                    yjbxxlcxxservice.insert(obj);
                    yjbxxService.update(jbxx);
                    yjbxxlcxxservice.clearCache();
                }
            });
            return Result.success("审核成功！");
        } catch (Exception e) {
            return Result.error("system.error");
        }
    }

    private String getUploadPathConfig() throws Exception {
        String uploadPath;
        //系统没开启配置,上传到项目路径
        if ("false".equals(Globals.MyConfig.getOrDefault("IsConfigUploadPath", "false"))){
            uploadPath= UploadController.class.getClassLoader().getResource("").getPath()+"upload";
        }else if (
                ("true".equals(Globals.MyConfig.getOrDefault("IsConfigUploadPath", "false")))&&(Globals.MyConfig.getOrDefault("ConfigUploadPath", null)!=null)
        ){
            uploadPath=Globals.MyConfig.get("ConfigUploadPath").toString();
        }else {
            throw new Exception("文件上传路径配置有误");
        }
        return uploadPath;
    }
}
