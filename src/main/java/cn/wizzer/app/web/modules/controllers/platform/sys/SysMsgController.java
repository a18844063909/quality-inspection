package cn.wizzer.app.web.modules.controllers.platform.sys;

import cn.wizzer.app.bus.modules.models.BusCharacter;
import cn.wizzer.app.sys.modules.models.Sys_msg;
import cn.wizzer.app.sys.modules.models.Sys_msg_user;
import cn.wizzer.app.sys.modules.models.Sys_user;
import cn.wizzer.app.sys.modules.services.SysMsgService;
import cn.wizzer.app.sys.modules.services.SysMsgUserService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.ext.websocket.WkNotifyService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.PageUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import cn.wizzer.framework.page.Pagination;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.nutz.lang.Times;
import org.nutz.lang.util.NutMap;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@IocBean
@At("/platform/sys/msg")
public class SysMsgController {
    private static final Log log = Logs.get();
    @Inject
    private SysMsgService sysMsgService;
    @Inject
    private SysMsgUserService sysMsgUserService;
    @Inject
    private SysUserService sysUserService;
    @Inject
    private WkNotifyService wkNotifyService;

    @At({"/", "/list/?"})
    @Ok("beetl:/platform/sys/msg/index.html")
    @RequiresPermissions("sys.manager.msg")
    public void index(String type, HttpServletRequest req) {
        req.setAttribute("status", Strings.isBlank(type) ? "" : "1");
        req.setAttribute("type", Strings.isBlank(type) ? "all" : type);
    }

    @At
    @Ok("json:full")
    @RequiresPermissions("sys.manager.msg")
    public Object data(@Param("searchType") String searchType, @Param("title") String title, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize,
                       @Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy,@Param("status") String status) {
        try {
            String sql = "select * from sys_msg where delFlag = 0 ";
            if (Strings.isNotBlank(searchType) && !"all".equals(searchType)) {
                sql +=" and type = '"+searchType+"'";
            }
            if (Strings.isNotBlank(status)) {
                sql +=" and status = '"+status+"'";
            }
            if (Strings.isNotBlank(title)) {
                sql +=" and title like '%"+title+"%'";
            }
            //sql +=StringUtil.dataScopeFilter("");
            sql+=" and opBy = '"+StringUtil.getPlatformUid()+"'";
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                if ("ascending".equalsIgnoreCase(pageOrderBy)) {
                    sql+= " order by "+pageOrderName+" asc ";
                } else {
                    sql+= " order by "+pageOrderName+" desc ";
                }
            }
            List<Map> mapList = new ArrayList<>();
            Pagination pagination = sysMsgService.listPageMap(pageNumber, pageSize, Sqls.create(sql));
            for (Object msg : pagination.getList()) {
                NutMap map = Lang.obj2nutmap(msg);
                map.put("all_num", sysMsgUserService.count(Cnd.where("msgId", "=", map.get("id", ""))));
                map.put("unread_num", sysMsgUserService.count(Cnd.where("msgId", "=", map.get("id", "")).and("status", "=", 0)));
                mapList.add(map);
            }
            pagination.setList(mapList);
            return Result.success().addData(pagination);
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At
    @Ok("json:full")
    @RequiresPermissions("sys.manager.msg")
    public Object user_view_data(@Param("type") String type, @Param("id") String id, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize, @Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy) {
        try {
            Cnd cnd = Cnd.NEW();
            String sql = "SELECT a.loginname,a.username,a.mobile,a.email,a.disabled,a.unitid,b.name as unitname,c.status,c.readat FROM sys_user a,sys_unit b,sys_msg_user c WHERE a.unitid=b.id \n" +
                    "and a.loginname=c.loginname and c.msgId='" + id + "' ";
            if (Strings.isNotBlank(type) && "unread".equals(type)) {
                sql += " and c.status=0 ";
            }
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                sql += " order by a." + pageOrderName + " " + PageUtil.getOrder(pageOrderBy);
            }
            return Result.success().addData(sysMsgService.listPage(pageNumber, pageSize, Sqls.create(sql)));
        } catch (Exception e) {
            return Result.error();
        }
    }
    @At("/detail/?")
    @Ok("json")
    public Object detail(String id, HttpServletRequest req) {
        try {
            Sys_msg sysMsg = sysMsgService.fetch(id);

            return Result.success().addData(sysMsg);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At("/detailUser/?")
    @Ok("json")
    public Object detailUser(String id, HttpServletRequest req) {
        try {
            List<Sys_user> users = sysUserService.listEntity(Sqls.create("select u.* from sys_msg_user mu  left join sys_user u  on mu.loginname = u.loginname where mu.msgId='"+id+"'"));
            return Result.success().addData(users);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error();
        }
    }
    @At("/addDo")
    @Ok("json")
    @RequiresPermissions("sys.manager.msg.add")
    @SLog(tag = "站内消息", msg = "${args[0].title}")
    public Object addDo(@Param("..") NutMap nutMap, HttpServletRequest req) {
        try {
            Sys_msg sysMsg = nutMap.getAs("msg", Sys_msg.class);
            sysMsg.setNote(nutMap.getString("note", ""));
            if(sysMsg.getStatus().equals("1")){
                sysMsg.setSendAt(Times.getTS());
            }
            sysMsg.setOpBy(StringUtil.getPlatformUid());
            String[] users = StringUtils.split(nutMap.getString("users", ""), ",");
            Sys_msg sys_msg = sysMsgService.saveMsg(sysMsg, users);
            if (sys_msg != null && sysMsg.getStatus().equals("1")) {
                wkNotifyService.notify(sys_msg, users);
            }
            sysMsgUserService.clearCache();
            return Result.success("操作成功！");
        } catch (Exception e) {
            return Result.error("操作失败");
        }
    }
    @At("/msgDo")
    @Ok("json")
    @SLog(tag = "发送消息", msg = "${msg.id}")
    public Object msgDo(@Param("..") Sys_msg msg, HttpServletRequest req) {
        try {
            Sys_msg sys_msg = sysMsgService.fetch(msg.getId());
            sys_msg.setStatus("1");
            sys_msg.setSendAt(Times.getTS());
            sys_msg.setOpBy(StringUtil.getPlatformUid());
            sysMsgService.update(sys_msg);
            List<Sys_msg_user> msgUsers =  sysMsgUserService.listEntity(Sqls.create(" select * from sys_msg_user where msgId = '"+msg.getId() +"' and delFlag = '0'"));
            if(msgUsers.size()>0){
                String user = "";
                for(Sys_msg_user  sysMsg_user : msgUsers){
                    user += ","+sysMsg_user.getLoginname();
                }
                user.substring(0, user.length()-1);
                String[] users = user.split(",");
                wkNotifyService.notify(sys_msg, users);
                sysMsgUserService.clearCache();
            }
            return Result.success("发送成功！");
        } catch (Exception e) {
            return Result.error("发送失败！");
        }
    }
    @At("/editFile")
    @Ok("json")
    @SLog(tag = "修改发文附件", msg = "${msg.id}")
    public Object editFile(@Param("..") Sys_msg msg, HttpServletRequest req) {
        try {
            Sys_msg sys_msg = sysMsgService.fetch(msg.getId());
            sys_msg.setFileId(msg.getFileId());
            sysMsgService.update(sys_msg);
            return Result.success("");
        } catch (Exception e) {
            return Result.error("");
        }
    }

    @At
    @Ok("json:{locked:'password|salt',ignoreNull:false}")
    public Object user_data(@Param("searchUnit") String searchUnit, @Param("searchName") String searchName,@Param("users") String users, @Param("searchKeyword") String searchKeyword, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize, @Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy) {
        try {
            Cnd cnd = Cnd.NEW().and("delFlag", "=", false).and("disabled", "=", false);
            if (Strings.isNotBlank(searchName) && Strings.isNotBlank(searchKeyword)) {
                cnd.and(searchName, "like", "%" + searchKeyword + "%");
            }
            if (Strings.isNotBlank(users) && Strings.isNotBlank(users)) {
                cnd.and("loginname", "not in",users);
            }
            if (Strings.isNotBlank(searchUnit) && Strings.isNotBlank(searchUnit)) {
                String units = Arrays.stream(searchUnit.split(",")).map(t -> "'"+t+"'").collect(Collectors.joining(","));
                cnd.and("unitid", " in ",units);
            }
            if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
                cnd.orderBy(pageOrderName, PageUtil.getOrder(pageOrderBy));
            }
            return Result.success().addData(sysUserService.listPageLinks(pageNumber, pageSize, cnd, "unit"));
        } catch (Exception e) {
            return Result.error();
        }
    }

    @At({"/delete/?"})
    @Ok("json")
    @RequiresPermissions("sys.manager.msg.delete")
    @SLog(tag = "站内消息", msg = "站内信标题:${req.getAttribute('title')}")
    public Object delete(String id, HttpServletRequest req) {
        try {
            req.setAttribute("title", sysMsgService.fetch(id).getTitle());
            sysMsgService.deleteMsg(id);
            sysMsgUserService.clearCache();
            return Result.success();
        } catch (Exception e) {
            return Result.error();
        }
    }


}
