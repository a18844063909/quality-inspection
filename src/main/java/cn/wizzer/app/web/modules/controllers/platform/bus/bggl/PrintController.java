package cn.wizzer.app.web.modules.controllers.platform.bus.bggl;

import cn.wizzer.app.bus.modules.models.YJbxx;
import cn.wizzer.app.bus.modules.models.YJbxxLcxx;
import cn.wizzer.app.bus.modules.services.YJbxxLcxxService;
import cn.wizzer.app.bus.modules.services.YJbxxService;
import cn.wizzer.app.sys.modules.services.SysDictService;
import cn.wizzer.app.sys.modules.services.SysUnitService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.DateUtil;
import cn.wizzer.app.web.commons.utils.ShiroUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.aop.interceptor.ioc.TransAop;
import org.nutz.dao.Sqls;
import org.nutz.ioc.aop.Aop;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Times;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;


/**
 * 报告审核  虞秀斌  2019-11
 */
@IocBean
@At("/platform/bus/bggl")
public class PrintController {
    private static final Log log = Logs.get();
    @Inject
    private YJbxxService yjbxxService;
    @Inject
    private YJbxxLcxxService yjbxxlcxxservice;
    @Inject
    private SysUnitService sysUnitService;
    @Inject
    private SysDictService sysDictService;
    @Inject
    private SysUserService sysUserService;
    @Inject
    private ShiroUtil shiroUtil;


    @Aop(TransAop.READ_COMMITTED)
    @At
    @Ok("json")
    @RequiresPermissions("bus.bggl")
    @SLog(tag = "报告打印", msg = "样品:${args[0].ypbh}")
    public Object printDo(@Param("..") YJbxx obj) {
        try {
            YJbxx jbxx = yjbxxService.fetch(obj.getId());
            jbxx.setLczt("13");
            jbxx.setDyzt("1");
            jbxx.setDysj(DateUtil.getDateTime());
            int dycs = Integer.valueOf(!StringUtils.isEmpty(jbxx.getDycs())?jbxx.getDycs():"0")+1;
            jbxx.setDycs(dycs+"");
            YJbxxLcxx lcxx = new YJbxxLcxx();
            lcxx.setLczt(jbxx.getLczt());
            lcxx.setJbxxId(jbxx.getId());
            lcxx.setSpyj("打印报告");
            lcxx.setOpBy(StringUtil.getPlatformUid());
            lcxx.setOpAt(Times.getTS());

            yjbxxlcxxservice.execute(Sqls.create("update y_jbxx_lcxx set delflag=1 where lczt>="+jbxx.getLczt()+" and jbxxid='"+jbxx.getId()+"'"));
            yjbxxlcxxservice.insert(lcxx);
            yjbxxService.update(jbxx);
            yjbxxlcxxservice.clearCache();
            return Result.success("打印标记成功！");
        } catch (Exception e) {
            return Result.error("system.error");
        }
    }
}
