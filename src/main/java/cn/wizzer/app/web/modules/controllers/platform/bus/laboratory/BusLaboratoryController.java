package cn.wizzer.app.web.modules.controllers.platform.bus.laboratory;

import cn.wizzer.app.bus.modules.models.equipment.BusEquipment;
import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentInspection;
import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentInspectionInventory;
import cn.wizzer.app.bus.modules.models.equipment.BusEquipmentProcess;
import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratory;
import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratoryEquipment;
import cn.wizzer.app.bus.modules.models.laboratory.BusLaboratoryUser;
import cn.wizzer.app.bus.modules.services.*;
import cn.wizzer.app.sys.modules.services.SysDictService;
import cn.wizzer.app.sys.modules.services.SysUnitService;
import cn.wizzer.app.sys.modules.services.SysUserService;
import cn.wizzer.app.web.commons.slog.annotation.SLog;
import cn.wizzer.app.web.commons.utils.ShiroUtil;
import cn.wizzer.app.web.commons.utils.StringUtil;
import cn.wizzer.framework.base.Result;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.nutz.aop.interceptor.ioc.TransAop;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.ioc.aop.Aop;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.lang.Times;
import org.nutz.lang.util.NutMap;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author tcjinxiu.com
 * @time 2024-04-11 10:19:44
 *
 */
@IocBean
@At("/platform/bus/laboratory")
public class BusLaboratoryController {
private static final Log log = Logs.get();
	@Inject
	BusLaboratoryService busLaboratoryService;
	@Inject
	BusLaboratoryEquipmentService busLaboratoryEquipmentService;
	@Inject
	BusLaboratoryUserService busLaboratoryUserService;
	@Inject
	private SysUserService sysUserService;
	@Inject
	private SysUnitService sysUnitService;

	@At("")
	@Ok("beetl:/platform/bus/laboratory/index.html")
	@RequiresAuthentication
	public void index(HttpServletRequest req) {
		req.setAttribute("userId",StringUtil.getPlatformUid());
		req.setAttribute("unitOptions", JSONArray.fromObject(sysUnitService.getJybmTree()));
		req.setAttribute("userOptions", JSONArray.fromObject(sysUserService.getUserS("")));
	}

	@At
	@Ok("json:full")
	@RequiresAuthentication
	public Object data(@Param("major") String major, @Param("name") String name,
					   @Param("pageNumber") int pageNumber,@Param("pageSize") int pageSize
			, @Param("pageOrderName") String pageOrderName, @Param("pageOrderBy") String pageOrderBy) {
		try {
			String sql = "select * from bus_laboratory where delFlag=0 ";
			if(StringUtils.isNotBlank(name)){
				sql+= " and  name like '%"+name+"%' ";
			}
			if(StringUtils.isNotBlank(major)){
				sql+= " and  major = '"+major+"' ";
			}
			//数据权限
			sql+= StringUtil.dataScopeFilter("");

			if (Strings.isNotBlank(pageOrderName) && Strings.isNotBlank(pageOrderBy)) {
				if ("ascending".equalsIgnoreCase(pageOrderBy)) {
					sql+= " order by "+pageOrderName+" asc,opAt asc ";
				} else {
					sql+= " order by "+pageOrderName+" desc,opAt asc ";
				}
			}else{
				sql+= " order by opAt asc ";
			}
			return Result.success().addData(busLaboratoryService.listPage(pageNumber, pageSize, Sqls.create(sql)));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Result.error();
		}
	}

	@Aop(TransAop.READ_COMMITTED)
	@At
	@Ok("json")
	@RequiresPermissions("laboratory.add")
	@SLog(tag = "Add", msg = "Add:bus_laboratory")
	public Object addDo(@Param("laboratory") BusLaboratory laboratory, HttpServletRequest req) {
		try {
			laboratory.setOpBy(StringUtil.getPlatformUid());
			laboratory.setOpAt(Times.getTS());
			laboratory.setDelFlag(false);
			busLaboratoryService.insertWith(laboratory, null);
			return Result.success("system.success");
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error("system.error");
		}
	}
	@At("/detail/?")
	@Ok("json")
	public Object detail(String id) {
		BusLaboratory laboratory = busLaboratoryService.fetchLinks(busLaboratoryService.fetch(id),null);
		try {
			return Result.success("system.success").addData(laboratory);
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}

	@Aop(TransAop.READ_COMMITTED)
	@At
	@Ok("json")
	@RequiresPermissions("laboratory.edit")
	@SLog(tag = "Edit", msg = "Edit:bus_laboratory")
	public Object editDo(@Param("laboratory") BusLaboratory laboratory, HttpServletRequest req) {
		try {
			for(BusLaboratoryEquipment equipment :laboratory.getEquipments()){
				equipment.setLaboratoryId(laboratory.getId());
				equipment.setOpAt(Times.getTS());
				equipment.setOpBy(StringUtil.getPlatformUid());
				equipment.setDelFlag(false);
				busLaboratoryEquipmentService.insertOrUpdate(equipment);
			}
			for (BusLaboratoryUser user : laboratory.getUsers()){
				user.setLaboratoryId(laboratory.getId());
				user.setOpAt(Times.getTS());
				user.setOpBy(StringUtil.getPlatformUid());
				user.setDelFlag(false);
				busLaboratoryUserService.insertOrUpdate(user);
			}
			busLaboratoryService.updateIgnoreNull(laboratory);
			return Result.success("system.success");
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}




	@At({"/delete","/delete/?"})
	@Ok("json")
	@RequiresPermissions("laboratory.delete")
	@SLog(tag = "Delete", msg = "Delete:bus_laboratory")
	public Object delete(String id,@Param("ids") String[] ids, HttpServletRequest req) {
		try {
			if(ids!=null&&ids.length>0){
				busLaboratoryService.vDelete(ids);
			}else{
				busLaboratoryService.vDelete(id);
			}
			return Result.success("system.success");
		} catch (Exception e) {
			return Result.error("system.error");
		}
	}

}
