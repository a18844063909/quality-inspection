package cn.wizzer.app.sys.modules.services.impl;

import cn.wizzer.app.sys.modules.models.UreportFileTbl;
import cn.wizzer.app.sys.modules.services.UreportFileTblService;
import cn.wizzer.framework.base.service.BaseServiceImpl;
import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.plugins.wkcache.annotation.CacheDefaults;
import org.nutz.plugins.wkcache.annotation.CacheRemoveAll;


/**
 * Created by wizzer on 2016/12/22.
 */
@IocBean(args = {"refer:dao"})
@CacheDefaults(cacheName = "ureport_file_tbl")
public class UreportFileTblServiceImpl extends BaseServiceImpl<UreportFileTbl> implements UreportFileTblService {
    public UreportFileTblServiceImpl(Dao dao) {
        super(dao);
    }

    @CacheRemoveAll
    public void clearCache() {

    }

}
