package cn.wizzer.app.sys.modules.services;

import cn.wizzer.app.sys.modules.models.Sys_menu;
import cn.wizzer.app.sys.modules.models.Sys_user;
import cn.wizzer.app.sys.modules.models.UreportFileTbl;
import cn.wizzer.framework.base.service.BaseService;
import org.nutz.lang.util.NutMap;

import java.util.List;
import java.util.Map;

/**
 * Created by wizzer on 2016/12/22.
 */
public interface UreportFileTblService extends BaseService<UreportFileTbl> {

    /**
     * 清空缓存
     */
    void clearCache();

}
