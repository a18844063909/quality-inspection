package cn.wizzer.app.sys.modules.models;

import cn.wizzer.framework.base.model.BaseModel;
import lombok.Data;
import org.nutz.dao.entity.annotation.*;

import java.io.Serializable;

@Table("ureport_file_tbl")
@Data
public class UreportFileTbl extends BaseModel implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Column
    @Name
    @Comment("ID")
    @ColDefine(type = ColType.VARCHAR, width = 32)
    @Prev(els = {@EL("uuid()")})
    private String id;

    @Column
    @Comment("表名")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String name;


    @Column
    @Comment("内容")
    @ColDefine(type = ColType.TEXT)
    private byte[] content;
}
